﻿using System;

namespace Task01._2
{
    /// <inheritdoc />
    /// <summary>
    ///     The MatrixElementChangedArgs class for passing event information.
    /// </summary>
    public class MatrixElementChangedArgs<T> : EventArgs
    {
        public T NewValue { get; }
        public T OldValue { get; }
        public int IIndex { get; }
        public int JIndex { get; }

        public MatrixElementChangedArgs(T newValue, T oldValue, int iIndex, int jIndex)
        {
            NewValue = newValue;
            OldValue = oldValue;
            IIndex = iIndex;
            JIndex = jIndex;
        }
    }
}