﻿using System;
using System.Text;

namespace Task01._2
{
    /// <summary>
    ///     The SquareMatrix class for
    ///     representing a square matrix.
    /// </summary>
    public class SquareMatrix<T>
    {
        protected T[] _matrix;
        private int _size;
        
        public int Size
        {
            get => _size;
            private set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Size must be more than 0");
                }
                _size = value;
            }
        }

        public T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);
                return GetValue(i, j);
            }
            set
            {
                CheckIndexes(i, j);
                SetNewValue(i, j, value);
            }
        }

        public event EventHandler<MatrixElementChangedArgs<T>> MatrixElementChanged;

        public SquareMatrix(int size)
        {
            Size = size;
            _matrix = new T[size * size];
        }

        protected virtual void SetNewValue(int i, int j, T value)
        {
            var oldValue = _matrix[i * Size + j];
            if (!Equals(value, oldValue))
            {
                _matrix[i * Size + j] = value;
                OnMatrixElementChanged(new MatrixElementChangedArgs<T>(value, oldValue, i, j));
            }
        }

        protected virtual T GetValue(int i, int j)
        {
            return _matrix[i * Size + j];
        }

        /// <summary>
        ///     Checks whether indexes are out of range.
        /// </summary>
        protected virtual void CheckIndexes(int i, int j)
        {
            if (i < 0 || j < 0 || i > Size || j > Size)
            {
                throw new IndexOutOfRangeException("Index out of range");
            }
        }

        /// <summary>
        ///     Event generation.
        /// </summary>
        /// <param name="matrixElementChangedArgs">To transmit event information.</param>
        protected void OnMatrixElementChanged(MatrixElementChangedArgs<T> matrixElementChangedArgs)
        {
            MatrixElementChanged?.Invoke(this, matrixElementChangedArgs);
        }

        /// <summary>
        ///     Returns matrix as a string.
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    sb.Append(this[i, j] + " ");
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }
    }
}