﻿namespace Task01._2
{
    /// <inheritdoc />
    /// <summary>
    ///     The DiagonalMatrix class for
    ///     representing a diagonal matrix.
    /// </summary>
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        public DiagonalMatrix(int size) : base(size)
        {
            _matrix = new T[size];
        }

        protected override void SetNewValue(int i, int j, T value)
        {
            if (i != j)
            {
                return;
            }
            var oldValue = _matrix[i];
            if (!Equals(value, oldValue))
            {
                _matrix[i] = value;
                OnMatrixElementChanged(new MatrixElementChangedArgs<T>(value, oldValue, i, j));
            }
        }

        protected override T GetValue(int i, int j)
        {
            return i != j ? default(T) : _matrix[i];
        }
    }
}