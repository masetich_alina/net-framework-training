﻿using System;

namespace Task01._2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var matrix = new SquareMatrix<int>(-2);
            Console.WriteLine(matrix.ToString());
            
            matrix.MatrixElementChanged += Show_Message;
            matrix.MatrixElementChanged += delegate (object sender, MatrixElementChangedArgs<int> e)
            {
                Console.WriteLine(e.OldValue + "=>" + e.NewValue);
            };
            matrix.MatrixElementChanged += (sender, e) => { Console.WriteLine(e.OldValue + "=>" + e.NewValue); };

            matrix[0, 0] = 18;
            Console.WriteLine(matrix.ToString());

            var dmatrix = new DiagonalMatrix<int>(3);
            Console.WriteLine(dmatrix.ToString());

            dmatrix.MatrixElementChanged += Show_Message;
            dmatrix.MatrixElementChanged += delegate (object sender, MatrixElementChangedArgs<int> e)
            {
                Console.WriteLine(e.OldValue + "=>" + e.NewValue);
            };
            dmatrix.MatrixElementChanged += (sender, e) => { Console.WriteLine(e.OldValue + "=>" + e.NewValue); };

            dmatrix[1, 1] = 5;
            Console.WriteLine(dmatrix.ToString());
        }

        private static void Show_Message<T>(object sender, MatrixElementChangedArgs<T> e)
        {
            Console.WriteLine(e.OldValue + "=>" + e.NewValue);
        }
    }
}