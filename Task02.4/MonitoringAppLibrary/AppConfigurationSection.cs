﻿using System.Configuration;

namespace MonitoringAppLibrary 
{
    public class AppConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("email")]
        public EmailConfigurationElement Email => (EmailConfigurationElement)base["email"]; 
    }
}
