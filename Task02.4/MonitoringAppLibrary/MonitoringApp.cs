﻿using System.Collections.Generic;
using System.IO;

namespace MonitoringAppLibrary
{
    public class MonitoringApp
    {
        private static readonly List<Monitoring> Monitoring = new List<Monitoring>();

        public MonitoringApp()
        {
            Helper.Handler();
            CheckChangeSettings();
            Check();
        }

        public static void Add(Monitoring monitoring)
        {
            Monitoring.Add(monitoring);
        }

        private static void Check()
        {
            foreach (var m in Monitoring)
            {
                m.StartTimer();
            }
        }

        private static void CheckChangeSettings()
        {
            var fileSystemWatcher = new FileSystemWatcher();
            fileSystemWatcher.Changed += FileSystemWatcher_Changed;
            fileSystemWatcher.Path = Directory.GetCurrentDirectory();
            fileSystemWatcher.Filter = "*exe.config";
            fileSystemWatcher.NotifyFilter = NotifyFilters.LastAccess
                                             | NotifyFilters.LastWrite
                                             | NotifyFilters.FileName
                                             | NotifyFilters.DirectoryName
                                             | NotifyFilters.Size
                                             | NotifyFilters.CreationTime;

            fileSystemWatcher.EnableRaisingEvents = true;
        }

        private static void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            foreach (var m in Monitoring)
            {
                m.EndTimer();
            }
            Monitoring.Clear();
            Helper.Handler();
            Check();
        }
    }
}