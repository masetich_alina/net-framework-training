﻿using System;
using System.Configuration;

namespace MonitoringAppLibrary
{
    public class EmailConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("address", IsRequired = true)] 
        public string Address
        {
            get => Convert.ToString(this["address"]);
            set => this["address"] = value;
        }

        [ConfigurationProperty("subject", IsRequired = true)]
        public string Subject 
        {
            get => Convert.ToString(this["subject"]);
            set => this["subject"] = value;
        }

        [ConfigurationProperty("body", IsRequired = true)]
        public string Body
        {
            get => Convert.ToString(this["body"]);
            set => this["body"] = value;
        }

        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get => Convert.ToString(this["password"]);
            set => this["password"] = value;
        }

        [ConfigurationProperty("monitoring")]
        [ConfigurationCollection(typeof(MonitoringConfigurationElementCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]

        public MonitoringConfigurationElementCollection Monitoring
        {
            get => this["monitoring"] as MonitoringConfigurationElementCollection;
            set => this["monitoring"] = value;
        }
    }
}
