﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MonitoringAppLibrary
{
    public class Email
    {

        public static string Address { get; set; }
        public static string Subject { get; set; }
        public static string Body { get; set; }
        public static string Password { get; set; }

        public Email(string email, string subject, string body, string password)
        {
            Address = email;
            Subject = subject;
            Body = body;
            Password = password;
        }

        public static async Task SendEmailAsync(string email, string url)
        {
            var from = new MailAddress(Address);
            var to = new MailAddress(email);
            var m = new MailMessage(from, to) {Subject = Subject, Body = Body + ' ' + url};
            var smtp = new SmtpClient("smtp.mail.ru", 587)
            {
                Credentials = new NetworkCredential(Address, Password), EnableSsl = true
            };
            await smtp.SendMailAsync(m);
        }
    }
}