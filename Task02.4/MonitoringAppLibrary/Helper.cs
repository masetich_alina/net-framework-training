﻿using System.Configuration;

namespace MonitoringAppLibrary
{
    public class Helper
    {
        public static void Handler()
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var _section = (AppConfigurationSection) configFile.GetSection("app");
            var email = new Email(_section.Email.Address, _section.Email.Subject, _section.Email.Body,
                _section.Email.Password);
            foreach (MonitoringConfigurationElement m in _section.Email.Monitoring)
            {
                MonitoringApp.Add(new Monitoring(m.Interval, m.Wait, m.Url, m.SendTo));
            }
        }
    }
}