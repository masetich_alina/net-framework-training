﻿using System.Net;
using System.Threading;
using NLog;

namespace MonitoringAppLibrary
{
    public class Monitoring
    {
        private Timer _timer;

        public int Interval { get; }
        public int Wait { get; }
        public string Url { get; }
        public string SendTo { get; }

        public Monitoring(int interval, int wait, string url, string email)
        {
            Interval = interval;
            Wait = wait;
            Url = url;
            SendTo = email;
        }

        public void StartTimer()
        {
            _timer = new Timer(Request, null, 0, Interval * 1000);
        }

        public void EndTimer()
        {
            _timer.Dispose();
        }

        private void Request(object state)
        {
            try
            {
                var request = WebRequest.Create(Url);
                request.Timeout = Wait * 1000;
                var response = request.GetResponse();
                response.Close();
                var logger = LogManager.GetCurrentClassLogger();
                logger.Info("Access to the site " + Url);
            }
            catch
            {
                Email.SendEmailAsync(SendTo, Url).GetAwaiter();
            }
        }
    }
}