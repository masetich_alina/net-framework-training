﻿using System;
using System.Configuration;

namespace MonitoringAppLibrary
{
    public class MonitoringConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("interval", IsRequired = true)]
        public int Interval 
        {
            get => Convert.ToInt32(this["interval"]);
            set => this["interval"] = value;
        }

        [ConfigurationProperty("wait", IsRequired = true)]
        public int Wait
        {
            get => Convert.ToInt32(this["wait"]);
            set => this["wait"] = value;
        }

        [ConfigurationProperty("url", IsRequired = true)]
        public string Url
        {
            get => Convert.ToString(this["url"]);
            set => this["url"] = value;
        }

        [ConfigurationProperty("sendTo", IsRequired = true)]
        public string SendTo
        {
            get => Convert.ToString(this["sendTo"]);
            set => this["sendTo"] = value;
        }

    }
}
