﻿using System.Configuration;

namespace MonitoringLibrary
{
    public class AppConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("email")]
        public EmailConfigurationElement Email => (EmailConfigurationElement)base["email"]; 
    }
}
