﻿using System.Configuration;

namespace MonitoringLibrary
{
    public class MonitoringConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new MonitoringConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as MonitoringConfigurationElement).Url;
        }
    }
}
