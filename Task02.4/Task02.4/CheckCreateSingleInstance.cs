﻿using System;
using System.Threading;

namespace Task02._4
{
    public static class CheckCreateSingleInstance
    {
        private static Mutex _mSingleton;
        public static void CheckingCreateSingleInstance()
        {
            _mSingleton = new Mutex(true, @"Global\MyApplicationMutex", out var createdNew);
            if (!createdNew)
            {
               throw new Exception("Already running, exiting");
            }
        }
    }
}
