﻿using System;
using MonitoringAppLibrary;

namespace Task02._4
{
    internal class Program
    {
        private static void Main()
        {
            CheckCreateSingleInstance.CheckingCreateSingleInstance();
            var monitoringApp = new MonitoringApp();
            Console.ReadKey();
        }
    }
}