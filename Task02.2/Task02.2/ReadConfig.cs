﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Task02._2
{
    public class ReadConfig
    {
        private static readonly XDocument XmlDoc = XDocument.Load("Config/XMLFile.xml", LoadOptions.SetLineInfo);

        public static int? ConvertToNullableInt(string s)
        {
            return string.IsNullOrEmpty(s) ? (int?) null : Convert.ToInt32(s);
        }

        public static IEnumerable<Login> GetConfig()
        {
            var config = XmlDoc.Descendants("login").Select(item => new Login
            {
                Name = item.Attribute("name")?.Value,
                Windows = item.Elements("window")
                    .Select(window => new Window
                    {
                        Title = window.Attribute("title")?.Value,
                        Top = ConvertToNullableInt(window.Element("top")?.Value),
                        Left = ConvertToNullableInt(window.Element("left")?.Value),
                        Width = ConvertToNullableInt(window.Element("width")?.Value),
                        Height = ConvertToNullableInt(window.Element("height")?.Value)
                    }).ToList()
            });
            return config;
        }
    }
}