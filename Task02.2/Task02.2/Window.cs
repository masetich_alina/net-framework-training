﻿namespace Task02._2
{
    public class Window
    {
        public string Title { get; set; }
        public int? Top { get; set; }
        public int? Left { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }

        public void SetNewValue()
        {
            Top = Top.GetValueOrDefault(0);
            Left = Left.GetValueOrDefault(0);
            Width = Width.GetValueOrDefault(400);
            Height = Height.GetValueOrDefault(150);
        }

        private static string SettingsWindowToString(int? value)
        {
            return value.HasValue ? value.ToString() : "?";
        }

        public override string ToString()
        {
            return Title + " ("
                         + SettingsWindowToString(Top) + ", "
                         + SettingsWindowToString(Left) + ", "
                         + SettingsWindowToString(Width) + ", "
                         + SettingsWindowToString(Height)
                         + ")";
        }
    }
}