﻿using System.Collections.Generic;

namespace Task02._2
{
    public class Login
    {
        public string Name { get; set; }
        public List<Window> Windows { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}