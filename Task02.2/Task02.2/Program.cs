﻿using System;

namespace Task02._2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var config = new Config();    
            Console.WriteLine();
            Console.WriteLine(config.GetSetupInformation());
            Console.WriteLine(config.GetIncorrectInformation());
            Console.WriteLine(ToJson.ConvertToJson());
        }
    }
}