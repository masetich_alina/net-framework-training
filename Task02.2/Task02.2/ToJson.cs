﻿using System.Text;
using Newtonsoft.Json;

namespace Task02._2
{
    public class ToJson
    {
        public static StringBuilder ConvertToJson()
        {
            var json = new StringBuilder();
            foreach (var item in ReadConfig.GetConfig())
            {
                foreach (var window in item.Windows)
                {
                    window.SetNewValue();
                }
                json.AppendLine(JsonConvert.SerializeObject(item));
            }

            return json;
        }
    }
}