﻿using System.Linq;
using System.Text;

namespace Task02._2
{
    public class Config
    {
        public StringBuilder GetIncorrectInformation()
        {
            var result = new StringBuilder();
            var res = ReadConfig.GetConfig().Where(item => item.Windows.All(w => w.Title != "main" ||
                                                                                 w.Top == null ||
                                                                                 w.Left == null ||
                                                                                 w.Width == null ||
                                                                                 w.Height == null));
            foreach (var login in res)
            {
                result.AppendLine(login.ToString());
            }
            return result;
        }

        public StringBuilder GetSetupInformation()
        {
            var result = new StringBuilder();

            foreach (var item in ReadConfig.GetConfig())
            {
                result.AppendLine("login: " + item.Name);
                foreach (var window in item.Windows)
                {
                    result.AppendLine(window.ToString());
                }
            }

            return result;
        }
    }
}