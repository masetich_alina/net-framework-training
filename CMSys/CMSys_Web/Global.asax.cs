﻿using System;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using CMSys_BLL_Services.Services;
using CMSys_Web.Util;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using System.Web;
using System.Web.Http;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_Web.Areas.Default.Models;

namespace CMSys_Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }

    public class MvcApplication : HttpApplication
    {
        private readonly IUserService _userService;

        public MvcApplication()
        {
            _userService = DependencyResolver.Current.GetService<IUserService>();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelValidatorProviders.Providers.Clear();

            NinjectModule serviceModule = new ServiceModule("DefaultConnection");
            NinjectModule userModule = new UserModule();
            var kernel = new StandardKernel(userModule, serviceModule);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }

        public override void Init()
        {
            PostAuthenticateRequest += MvcApplication_PostAuthenticateRequest;
            base.Init();
        }

        private void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null)
            {
                return;
            }
            var encTicket = authCookie.Value;
            if (string.IsNullOrEmpty(encTicket))
            {
                return;
            } 
            var ticket = FormsAuthentication.Decrypt(encTicket);
            var id = new UserIdentity(ticket);
            var userRoles = _userService.GetRolesForUser(id.Name);
            var prin = new GenericPrincipal(id, userRoles);
            HttpContext.Current.User = prin;
        }
    }
}
