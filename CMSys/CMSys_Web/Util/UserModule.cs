﻿using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Services;
using Ninject.Modules;

namespace CMSys_Web.Util
{
    public class UserModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>();
            Bind<ICourseService>().To<CourseService>();
            Bind<ICourseGroupService>().To<CourseGroupService>();
            Bind<ITrainerGroupService>().To<TrainerGroupService>();
            Bind<ITrainerService>().To<TrainerService>();
            Bind<IRoleService>().To<RoleService>();
        }
    }
}