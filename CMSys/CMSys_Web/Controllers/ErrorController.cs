﻿using System.Web.Mvc;

namespace CMSys_Web.Controllers
{
    [RoutePrefix("error")]
    public class ErrorController : Controller
    {
        [Route("notFound")]
        [HttpGet]
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }
    }
}