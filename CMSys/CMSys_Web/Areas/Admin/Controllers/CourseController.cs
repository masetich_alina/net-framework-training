﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_Web.Areas.Admin.Models;

namespace CMSys_Web.Areas.Admin.Controllers
{
    [RouteArea("Admin", AreaPrefix = "admin")]
    public class CourseController : Controller
    {
        private readonly ICourseService _courseService;
        private readonly ICourseGroupService _courseGroupService;
        private readonly ITrainerService _trainerService;

        public CourseController(ICourseService service, ICourseGroupService courseGroupService, ITrainerService trainerService)
        {
            _courseService = service;
            _courseGroupService = courseGroupService;
            _trainerService = trainerService;
        }

        [Route("courses")]
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var courseDto = _courseService.GetCourses();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseDto, CourseViewModel>()).CreateMapper();
            var courses = mapper.Map<IEnumerable<CourseDto>, List<CourseViewModel>>(courseDto);
            return View(courses);
        }
        [Route("course/details/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Details(Guid? id)
        {
            try
            {
                var courseDto = _courseService.GetCourse(id);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseDto, CourseViewModel>()).CreateMapper();
                var course = mapper.Map<CourseDto, CourseViewModel>(courseDto);
                return View(course);
            }
            catch (ValidationException ex)
            {
                return Content(ex.Message);
            }
        }

        [Route("course/create")]
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var courseGroups = new SelectList(_courseGroupService.GetCourseGroups(), "CourseGroupId", "CourseGroupName");
            ViewBag.CourseGroups = courseGroups;
            return View();
        }

        [Route("course/create")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(CourseViewModel course)
        {
            try
            {
                var courseDto = new CourseDto
                {
                    CourseId = Guid.NewGuid(),
                    CourseName = course.CourseName,
                    Description = course.Description,
                    IsNewCourse = course.IsNewCourse,
                    CodeType = course.CodeType,
                    CodePlanningMethod = course.CodePlanningMethod,
                    CourseGroupId = course.CourseGroupId
                };
                var courseGroups = new SelectList(_courseGroupService.GetCourseGroups(), "CourseGroupId", "CourseGroupName");
                ViewBag.CourseGroups = courseGroups;
                _courseService.CreateCourse(courseDto);
                return RedirectToAction("Index", "Course");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return View(course);
        }

        [Route("course/edit/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var courseDto = _courseService.GetCourse(id);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseDto, CourseViewModel>()).CreateMapper();
            var course = mapper.Map<CourseDto, CourseViewModel>(courseDto);
            if (courseDto == null)
            {
                return HttpNotFound();
            }
            var courseGroups = new SelectList(_courseGroupService.GetCourseGroups(), "CourseGroupId", "CourseGroupName");
            ViewBag.CourseGroups = courseGroups;
            return View(course);
        }

        [Route("course/edit/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(CourseViewModel course)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseViewModel, CourseDto>()).CreateMapper();
                var courseDto = mapper.Map<CourseViewModel, CourseDto>(course);
                var courseGroups = new SelectList(_courseGroupService.GetCourseGroups(), "CourseGroupId", "CourseGroupName");
                ViewBag.CourseGroups = courseGroups;
                _courseService.EditCourse(courseDto);
                return RedirectToAction("Index", "Course");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return View(course);
        }

        [Route("course/updateTrainers/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult UpdateTrainers(Guid? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var courseDto = _courseService.GetCourse(id);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseDto, CourseViewModel>()).CreateMapper();
            var course = mapper.Map<CourseDto, CourseViewModel>(courseDto);
            if (courseDto == null)
            {
                return HttpNotFound();
            }

            var trainersForSelectList = _trainerService.GetTrainers()
                .Where(x => course.Trainers.All(y => y.TrainerId != x.TrainerId));
            var trainers = new SelectList(trainersForSelectList, "TrainerId", "User.Fullname");
            ViewBag.Trainers = trainers;
            return View(course);
        }

        [Route("course/deleteTrainer/{courseId}/{trainerId}")]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteTrainer(Guid courseId, Guid trainerId)
        {
            var courseDto = _courseService.GetCourse(courseId);
            var trainerDto = _trainerService.GetTrainer(trainerId);
            _courseService.DeleteTrainerInCourse(courseDto.CourseId, trainerDto.TrainerId);
            return RedirectToAction("Index", "Course");
        }

        [Route("course/addTrainer")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult AddTrainer(CourseViewModel course)
        {
            _courseService.AddTrainerInCourse(course.CourseId, Guid.Parse(course.SelectedTrainerId));
            return RedirectToAction("Index", "Course");
        }

        [Route("course/delete/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Guid? id)
        {
            var courseDto = _courseService.GetCourse(id);
            if (courseDto == null)
            {
                return HttpNotFound();
            }
            try
            {
                _courseService.DeleteCourse(courseDto.CourseId);
               return RedirectToAction("Index", "Course");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseDto, CourseViewModel>()).CreateMapper();
                var course = mapper.Map<CourseDto, CourseViewModel>(courseDto);
                return PartialView(course);
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            _courseService.Dispose();
        }
    }
}