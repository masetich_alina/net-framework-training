﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_Web.Areas.Admin.Models;

namespace CMSys_Web.Areas.Admin.Controllers
{
    [RouteArea("Admin", AreaPrefix = "admin")]
    public class TrainerController : Controller
    {
        private readonly ITrainerService _trainerService;
        private readonly IUserService _userService;
        private readonly ITrainerGroupService _trainerGroupService;

        public TrainerController(ITrainerService trainerService, IUserService userService, ITrainerGroupService trainerGroupService)
        {
            _trainerService = trainerService;
            _userService = userService;
            _trainerGroupService = trainerGroupService;
        }

        [Route("trainers")]
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var trainerDto = _trainerService.GetTrainers();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerDto, TrainerViewModel>()).CreateMapper();
            var trainers = mapper.Map<IEnumerable<TrainerDto>, List<TrainerViewModel>>(trainerDto);
            return View(trainers);
        }

        [Route("trainer/create")]
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var trainers = new SelectList(_userService.GetUsers(), "UserId", "FullName");
            var trainerGroups = new SelectList(_trainerGroupService.GetTrainerGroups(), "TrainerGroupId", "TrainerGroupName");
            ViewBag.Trainers = trainers;
            ViewBag.TrainerGroups = trainerGroups;
            return View();
        }

        [Route("trainer/create")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(TrainerViewModel trainer)
        {
            try
            {
                var trainerDto = new TrainerDto
                {
                    TrainerId = Guid.NewGuid(),
                    TrainerInfo = trainer.TrainerInfo,
                    TrainerGroupId = trainer.TrainerGroupId,
                    UserId = trainer.UserId
                };

                _trainerService.CreateTrainer(trainerDto);
                return RedirectToAction("Index", "Trainer");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return View(trainer);
        }

        [Route("trainer/edit/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var trainerDto = _trainerService.GetTrainer(id);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerDto, TrainerViewModel>()).CreateMapper();
            var trainer = mapper.Map<TrainerDto, TrainerViewModel>(trainerDto);
            if (trainerDto == null)
            {
                return HttpNotFound();
            }
            var trainers = new SelectList(_userService.GetUsers(), "UserId", "FullName");
            var trainerGroups = new SelectList(_trainerGroupService.GetTrainerGroups(), "TrainerGroupId", "TrainerGroupName");
            ViewBag.Trainers = trainers;
            ViewBag.TrainerGroups = trainerGroups;
            return PartialView(trainer);
        }

        [Route("trainer/edit/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(TrainerViewModel trainer)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerViewModel, TrainerDto>()).CreateMapper();
                var trainerDto = mapper.Map<TrainerViewModel, TrainerDto>(trainer);
                _trainerService.EditTrainer(trainerDto);
                return RedirectToAction("Index", "Trainer");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return PartialView(trainer);
        }

        [Route("trainer/delete/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpGet, ActionName("Delete")]
        public ActionResult Delete(Guid? id)
        {
            var trainerDto = _trainerService.GetTrainer(id);
            if (trainerDto == null)
            {
                return HttpNotFound();
            }

            try
            {
                _trainerService.DeleteTrainer(trainerDto.TrainerId);
                return RedirectToAction("Index", "Trainer");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerDto, TrainerViewModel>()).CreateMapper();
                var trainer = mapper.Map<TrainerDto, TrainerViewModel>(trainerDto);
                return PartialView(trainer);
            }
        }

        protected override void Dispose(bool disposing)
        {
            _trainerService.Dispose();
        }
    }
}