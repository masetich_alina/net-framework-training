﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_Web.Areas.Admin.Models;

namespace CMSys_Web.Areas.Admin.Controllers
{
    [RouteArea("Admin", AreaPrefix = "admin")]
    public class RoleController : Controller
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService service)
        {
            _roleService = service;
        }

        [Route("roles")]
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var roleDto = _roleService.GetRoles();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<RoleDto, RoleViewModel>()).CreateMapper();
            var roles = mapper.Map<IEnumerable<RoleDto>, List<RoleViewModel>>(roleDto);
            return View(roles);
        }

        [Route("role/create")]
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return PartialView();
        }

        [Route("role/create")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(RoleViewModel role)
        {
            try
            {
                var rolDto = new RoleDto
                {
                    RoleId = Guid.NewGuid(),
                    RoleName = role.RoleName
                };

                _roleService.CreateRole(rolDto);
                return RedirectToAction("Index", "Role");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return PartialView(role);
        }

        [Route("role/edit/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var roleDto = _roleService.GetRole(id);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<RoleDto, RoleViewModel>()).CreateMapper();
            var courseGroup = mapper.Map<RoleDto, RoleViewModel>(roleDto);
            if (roleDto != null)
            {
                return PartialView(courseGroup);
            }
            return HttpNotFound();
        }

        [Route("role/edit/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(RoleViewModel role)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<RoleViewModel, RoleDto>()).CreateMapper();
                var roleDto = mapper.Map<RoleViewModel, RoleDto>(role);
                _roleService.EditRole(roleDto);
                return RedirectToAction("Index", "Role");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return PartialView(role);
        }

        [Route("role/delete{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Guid? id)
        {
            var roleDto = _roleService.GetRole(id);
            if (roleDto == null)
            {
                return HttpNotFound();
            }
            try
            {
                _roleService.DeleteRole(roleDto.RoleId);
                return RedirectToAction("Index", "Role");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<RoleDto, RoleViewModel>()).CreateMapper();
                var role = mapper.Map<RoleDto, RoleViewModel>(roleDto);
                return PartialView(role);
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            _roleService.Dispose();
        }

    }
}