﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_Web.Areas.Admin.Models;
using PagedList;

namespace CMSys_Web.Areas.Admin.Controllers
{
    [RouteArea("Admin", AreaPrefix = "admin")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;

        public UserController(IUserService service, IRoleService roleService)
        {
            _userService = service;
            _roleService = roleService;
        }

        [Route("users")]
        [Authorize(Roles = "Admin")]
        public ActionResult Index(int? page)
        {
            const int pageSize = 10;
            var pageNumber = (page ?? 1);
            var userDto = _userService.GetUsers();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, UserViewModel>()).CreateMapper();
            var users = mapper.Map<IEnumerable<UserDto>, List<UserViewModel>>(userDto);
            return View(users.ToPagedList(pageNumber, pageSize));
        }

        [Route("user/details{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Details(Guid? id)
        {
            try
            {
                var userDto = _userService.GetUser(id);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, UserViewModel>()).CreateMapper();
                var user = mapper.Map<UserDto, UserViewModel>(userDto);
                return PartialView(user);
            }
            catch (ValidationException ex)
            {
                return Content(ex.Message);
            }
        }

        [Route("user/create")]
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        [Route("user/create")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(UserViewModel user, HttpPostedFileBase image1)
        {
            try
            {
                if (image1 != null)
                {
                    user.Photo = new byte[image1.ContentLength];
                    image1.InputStream.Read(user.Photo, 0, image1.ContentLength);
                }

                var userDto = new UserDto
                {
                    UserId = Guid.NewGuid(),
                    Email = user.Email, 
                    PasswordHash = user.PasswordHash,
                    PasswordSalt = user.PasswordSalt,
                    Firstname = user.Firstname,
                    Lastname = user.Lastname,
                    StartDate = DateTime.Now,
                    EndDate = user.EndDate,
                    Department = user.Department,
                    OfficeLocation = user.OfficeLocation,
                    Position = user.Position,
                    Photo = user.Photo
                };

                _userService.CreateUser(userDto);
                return RedirectToAction("Index", "User");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return View(user);
        }

        [Route("user/edit/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var userDto = _userService.GetUser(id);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, UserViewModel>()).CreateMapper();
            var user = mapper.Map<UserDto, UserViewModel>(userDto);
            if (userDto != null)
            {
                return PartialView(user);
            }
            return HttpNotFound();
        }

        [Route("user/edit/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(UserViewModel user)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserViewModel, UserDto>()).CreateMapper();
                var userDto = mapper.Map<UserViewModel, UserDto>(user);
                _userService.EditUser(userDto);
                return RedirectToAction("Index", "User");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return PartialView(user);
        }

        [Route("user/delete/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Guid? id)
        {
            var userDto = _userService.GetUser(id);
            if (userDto == null)
            {
                return HttpNotFound();
            }

            try
            {
                _userService.DeleteUser(userDto.UserId);
                return RedirectToAction("Index", "Trainer");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, UserViewModel>()).CreateMapper();
                var user = mapper.Map<UserDto, UserViewModel>(userDto);
                return PartialView(user);
            }
        }

        [Route("user/updateRoles")]
        [Authorize(Roles = "Admin")]
        public ActionResult UpdateRoles(Guid id)
        {
            var userDto = _userService.GetUser(id);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, UserViewModel>()).CreateMapper();
            var user = mapper.Map<UserDto, UserViewModel>(userDto);
            var roles = new SelectList(_roleService.GetRoles(), "RoleId", "RoleName");
            ViewBag.Roles = roles;
            return View(user);
        }

        [Route("user/deleteRole/{userId}/{roleId}")]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteRole(Guid userId, Guid roleId)
        {
            _userService.DeleteRole(userId, roleId);
            return RedirectToAction("Index", "User"); 
        }
        [Route("user/addRole")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult AddRole(UserViewModel user) 
        {
            _userService.AddRole(user.UserId, Guid.Parse(user.SelectedRoleId));
            return RedirectToAction("Index", "User");
        }

        protected override void Dispose(bool disposing)
        {
            _userService.Dispose();
        }
    }
}