﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_Web.Areas.Admin.Models;

namespace CMSys_Web.Areas.Admin.Controllers.Api
{
    public class UserSearchController : ApiController
    {
        private readonly IUserService _userService;

        public UserSearchController(IUserService service)
        {
            _userService = service;
        }

        public IEnumerable<UserViewModel> GetUsers(string fullname)
        {
            var usersDto = _userService.GetUsersByFullname(fullname);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, UserViewModel>()).CreateMapper();
            var users = mapper.Map<IEnumerable<UserDto>, List<UserViewModel>>(usersDto);
            return users;
        }
    }
}
