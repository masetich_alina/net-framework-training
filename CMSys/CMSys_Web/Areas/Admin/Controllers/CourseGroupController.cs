﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_Web.Areas.Admin.Models;

namespace CMSys_Web.Areas.Admin.Controllers
{
    [RouteArea("Admin", AreaPrefix = "admin")]
    public class CourseGroupController : Controller
    {
        readonly ICourseGroupService _courseGroupService;

        public CourseGroupController(ICourseGroupService service)
        {
            _courseGroupService = service;
        }

        [Route("courseGroups")]
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var courseGroupDto = _courseGroupService.GetCourseGroups();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseGroupDto, CMSys_Web.Areas.Admin.Models.CourseGroupViewModel>()).CreateMapper();
            var courseGroups = mapper.Map<IEnumerable<CourseGroupDto>, List<CMSys_Web.Areas.Admin.Models.CourseGroupViewModel>>(courseGroupDto);
            return View(courseGroups);
        }

        [Route("courseGroup/create")]
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return PartialView();
        }

        [Route("courseGroup/create")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(CourseGroupViewModel courseGroup)
        {
            try
            {
                var courseGroupDto = new CourseGroupDto
                {
                    CourseGroupId = Guid.NewGuid(),
                    CourseGroupName = courseGroup.CourseGroupName
                };

                _courseGroupService.CreateCourseGroup(courseGroupDto);
                return RedirectToAction("Index", "CourseGroup");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return PartialView(courseGroup);
        }

        [Route("courseGroup/edit/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var courseGroupDto = _courseGroupService.GetCourseGroup(id);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseGroupDto, CourseGroupViewModel>()).CreateMapper();
            var courseGroup = mapper.Map<CourseGroupDto, CourseGroupViewModel>(courseGroupDto);
            if (courseGroupDto != null)
            {
                return PartialView(courseGroup);
            }
            return HttpNotFound();
        }

        [Route("courseGroup/edit/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(CourseGroupViewModel courseGroup)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseGroupViewModel, CourseGroupDto>()).CreateMapper();
                var courseGroupDto = mapper.Map<CourseGroupViewModel, CourseGroupDto>(courseGroup);
                _courseGroupService.EditCourseGroup(courseGroupDto);
                return RedirectToAction("Index", "CourseGroup");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }

            return PartialView(courseGroup);
        }

        [Route("courseGroup/delete/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Guid? id)
        {
            var courseGroupDto = _courseGroupService.GetCourseGroup(id);
            if (courseGroupDto == null)
            {
                return HttpNotFound();
            }
            try
            {
                _courseGroupService.DeleteCourseGroup(courseGroupDto.CourseGroupId);
                return RedirectToAction("Index", "CourseGroup");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseGroupDto, CourseGroupViewModel>()).CreateMapper();
                var courseGroup= mapper.Map<CourseGroupDto, CourseGroupViewModel>(courseGroupDto);
                return PartialView(courseGroup);
            }
        }

        protected override void Dispose(bool disposing)
        {
            _courseGroupService.Dispose();
        }
    }
}