﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_Web.Areas.Admin.Models;

namespace CMSys_Web.Areas.Admin.Controllers
{
    [RouteArea("Admin", AreaPrefix = "admin")]
    public class TrainerGroupController : Controller
    {
        readonly ITrainerGroupService _trainerGroupService;

        public TrainerGroupController(ITrainerGroupService service)
        {
            _trainerGroupService = service;
        }

        [Route("trainerGroups")]
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var trainerGroupDto = _trainerGroupService.GetTrainerGroups();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerGroupDto, TrainerGroupViewModel>())
                .CreateMapper();
            var trainerGroups = mapper.Map<IEnumerable<TrainerGroupDto>, List<TrainerGroupViewModel>>(trainerGroupDto); 
            return View(trainerGroups);
        }

        [Route("trainerGroups/create")]
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return PartialView();
        }

        [Route("trainerGroups/create")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(TrainerGroupViewModel trainerGroup)
        {
            try
            {
                var trainerGroupDto = new TrainerGroupDto
                {
                    TrainerGroupId = Guid.NewGuid(),
                    TrainerGroupName = trainerGroup.TrainerGroupName
                };

                _trainerGroupService.CreateTrainerGroup(trainerGroupDto);
                return RedirectToAction("Index", "TrainerGroup");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return PartialView(trainerGroup);
        }

        [Route("trainerGroups/edit/{id}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var trainerGroupDto = _trainerGroupService.GetTrainerGroup(id);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerGroupDto, TrainerGroupViewModel>()).CreateMapper();
            var trainerGroup = mapper.Map<TrainerGroupDto, TrainerGroupViewModel>(trainerGroupDto);
            if (trainerGroupDto != null)
            {
                return PartialView(trainerGroup);
            }
            return HttpNotFound();
        }

        [Route("trainerGroups/edit/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(TrainerGroupViewModel trainerGroup)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerGroupViewModel, TrainerGroupDto>()).CreateMapper();
                var trainerGroupDto = mapper.Map<TrainerGroupViewModel, TrainerGroupDto>(trainerGroup);
                _trainerGroupService.EditTrainerGroup(trainerGroupDto);
                return RedirectToAction("Index", "TrainerGroup");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return PartialView(trainerGroup);
        }

        [Route("trainerGroups/delete/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpGet, ActionName("Delete")]
        public ActionResult Delete(Guid? id)
        {
            var trainerGroupDto = _trainerGroupService.GetTrainerGroup(id);
            if (trainerGroupDto == null)
            {
                return HttpNotFound();
            }

            try
            {
                _trainerGroupService.DeleteTrainerGroup(trainerGroupDto.TrainerGroupId);
                return RedirectToAction("Index", "TrainerGroup");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerGroupDto, TrainerGroupViewModel>()).CreateMapper();
                var trainerGroup = mapper.Map<TrainerGroupDto, TrainerGroupViewModel>(trainerGroupDto);
                return PartialView(trainerGroup);
            }
        }

        protected override void Dispose(bool disposing)
        {
            _trainerGroupService.Dispose();
        }
    }
}
