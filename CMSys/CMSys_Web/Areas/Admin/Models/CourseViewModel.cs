﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMSys_Web.Areas.Admin.Models
{
    public class CourseViewModel
    {
        public Guid CourseId { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public string CourseName { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public bool IsNewCourse { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public int CodeType { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public int CodePlanningMethod { get; set; }
        public Guid CourseGroupId { get; set; }
        public CourseGroupViewModel CourseGroup { get; set; }

        [NotMapped]
        public string SelectedTrainerId { get; set; }

        public ICollection<TrainerViewModel> Trainers { get; set; }

        public CourseViewModel()
        {
            Trainers = new List<TrainerViewModel>();
        }
    }
}