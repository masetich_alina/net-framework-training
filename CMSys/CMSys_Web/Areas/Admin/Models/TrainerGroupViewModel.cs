﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMSys_Web.Areas.Admin.Models
{
    public class TrainerGroupViewModel
    {
        public Guid TrainerGroupId { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public string TrainerGroupName { get; set; }

        public ICollection<TrainerViewModel> Trainers { get; set; }
        public TrainerGroupViewModel()
        {
            Trainers = new List<TrainerViewModel>();
        }
    }
}