﻿using System;
using System.Collections.Generic;

namespace CMSys_Web.Areas.Admin.Models
{
    public class TrainerViewModel
    {
        public Guid TrainerId { get; set; }
        public string TrainerInfo { get; set; }

        public Guid UserId { get; set; }
        public UserViewModel User { get; set; }

        public Guid TrainerGroupId { get; set; }
        public TrainerGroupViewModel TrainerGroup { get; set; }

        public ICollection<CourseViewModel> Courses { get; set; }
        public TrainerViewModel()
        {
            Courses = new List<CourseViewModel>();
        }
    }
}