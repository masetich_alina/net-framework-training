﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMSys_Web.Areas.Admin.Models
{
    public class CourseGroupViewModel
    {
        public Guid CourseGroupId { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public string CourseGroupName { get; set; }

        public ICollection<CourseViewModel> Courses { get; set; }
        public CourseGroupViewModel()
        {
            Courses = new List<CourseViewModel>();
        }
    }
}