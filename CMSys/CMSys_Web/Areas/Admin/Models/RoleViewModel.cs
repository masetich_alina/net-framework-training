﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMSys_Web.Areas.Admin.Models
{
    public class RoleViewModel
    {
        public Guid RoleId { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public string RoleName { get; set; }

        public ICollection<UserViewModel> Users { get; set; }
        public RoleViewModel()
        {
            Users = new List<UserViewModel>();
        }
    }
}