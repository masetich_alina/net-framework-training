﻿using System.Security.Principal;
using System.Web.Security;

namespace CMSys_Web.Areas.Default.Models
{
    public class UserIdentity : IIdentity, IPrincipal
    {
        private readonly FormsAuthenticationTicket _ticket;

        public UserIdentity(FormsAuthenticationTicket ticket)
        {
            _ticket = ticket;
        }

        public string AuthenticationType => "User";

        public bool IsAuthenticated => true;

        public string Name => _ticket.Name;

        public string UserId => _ticket.UserData;

        public bool IsInRole(string role)
        {
            return Roles.IsUserInRole(role);
        }

        public IIdentity Identity => this;
    }
}