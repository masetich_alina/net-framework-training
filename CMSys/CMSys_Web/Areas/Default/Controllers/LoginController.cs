﻿using System;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_Web.Areas.Default.Models;

namespace CMSys_Web.Areas.Default.Controllers
{
    [RouteArea("Default", AreaPrefix = "login")]
    public class LoginController : Controller
    {
        private readonly IUserService _userService;

        public LoginController(IUserService service)
        {
            _userService = service;
        }

        [Route("login")]
        public ActionResult Login()
        {
            return View();
        }

        [Route("login")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                var user = _userService.Login(model.Email, model.Password);
                if (user != null)
                {
                    SetupFormsAuthTicket(model.Email, true);
                    return RedirectToAction("Index", "Course", new {area = "User"});
                }
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError("", "There is no user with such login and password");
            }
            return View(model);
        }

        [Route("logout")]
        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Login", new{area = "Default"});
        }

        private UserViewModel SetupFormsAuthTicket(string userName, bool persistanceFlag)
        {
            var userDto = _userService.GetUserByEmail(userName);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, UserViewModel>()).CreateMapper();
            var user = mapper.Map<UserDto, UserViewModel>(userDto);

            var userId = user.UserId;
            var userData = userId.ToString(CultureInfo.InvariantCulture.ToString());
            var authTicket = new FormsAuthenticationTicket(1, 
                userName, 
                DateTime.Now,            
                DateTime.Now.AddMinutes(30), 
                persistanceFlag, 
                userData);

            var encTicket = FormsAuthentication.Encrypt(authTicket);
            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
            return user;
        }
    }
}