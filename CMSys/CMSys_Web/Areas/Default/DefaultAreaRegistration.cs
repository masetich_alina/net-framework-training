﻿using System.Web.Mvc;

namespace CMSys_Web.Areas.Default
{
    public class DefaultAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Default";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
        }
    }
}