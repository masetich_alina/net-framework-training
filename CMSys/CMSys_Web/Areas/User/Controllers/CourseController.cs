﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_Web.Areas.User.Models;

namespace CMSys_Web.Areas.User.Controllers
{
    [RouteArea("User", AreaPrefix = "")]
    public class CourseController : Controller
    {
        private readonly ICourseService _courseService;
        private readonly IUserService _userService;

        public CourseController(ICourseService service, IUserService userService)
        {
            _courseService = service;
            _userService = userService;
        }

        [Route("")]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Index()
        {
            var courseDto = _courseService.GetCourses();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseDto, CourseViewModel>()).CreateMapper();
            var courses = mapper.Map<IEnumerable<CourseDto>, List<CourseViewModel>>(courseDto);
            return View(courses);
        }

        [Route("course/details/{id}")]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Details(Guid? id)
        {
            try
            {
                var courseDto = _courseService.GetCourse(id);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseDto, CourseViewModel>()).CreateMapper();
                var course = mapper.Map<CourseDto, CourseViewModel>(courseDto);
                return View(course);
            }
            catch (ValidationException ex)
            {
                return HttpNotFound();
            }
        }
        
        protected override void Dispose(bool disposing)
        {
           _courseService.Dispose();
        }
    }
}