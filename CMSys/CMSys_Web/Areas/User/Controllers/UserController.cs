﻿using System;
using System.Web.Mvc;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_Web.Areas.User.Models;

namespace CMSys_Web.Areas.User.Controllers
{
    [RouteArea("User", AreaPrefix = "user")]
    public class UserController : Controller
    {
        readonly IUserService _userService;

        public UserController(IUserService service)
        {
            _userService = service;
        }
        [Route("user/details/{id}")]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Details(Guid? id)
        {
            try
            {
                var userDto = _userService.GetUser(id);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, UserViewModel>()).CreateMapper();
                var user = mapper.Map<UserDto, UserViewModel>(userDto);
                return PartialView(user);
            }
            catch (ValidationException ex)
            {
                return Content(ex.Message);
            }
        }
    }
}