﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_Web.Areas.User.Models;

namespace CMSys_Web.Areas.User.Controllers
{
    [RouteArea("User", AreaPrefix = "user")]
    public class TrainerGroupController : Controller
    {
        readonly ITrainerGroupService _trainerGroupService;

        public TrainerGroupController(ITrainerGroupService service)
        {
            _trainerGroupService = service;
        }

        [Route("trainerGroups")]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Index()
        {
            var trainerGroupDto = _trainerGroupService.GetTrainerGroups();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerGroupDto, TrainerGroupViewModel>())
                .CreateMapper();
            var trainerGroups = mapper.Map<IEnumerable<TrainerGroupDto>, List<TrainerGroupViewModel>>(trainerGroupDto);
            return View(trainerGroups);
        }
    }
}