﻿using System.Web.Mvc;

namespace CMSys_Web.Areas.User
{
    public class UserAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "User";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
        }
    }
}