﻿using System;
using System.Collections.Generic;

namespace CMSys_Web.Areas.User.Models
{
    public class CourseGroupViewModel
    {
        public Guid CourseGroupId { get; set; }
        public string CourseGroupName { get; set; }

        public ICollection<CourseViewModel> Courses { get; set; }
        public CourseGroupViewModel()
        {
            Courses = new List<CourseViewModel>();
        }
    }
}