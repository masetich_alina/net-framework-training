﻿using System;
using System.Collections.Generic;

namespace CMSys_Web.Areas.User.Models
{
    public class TrainerGroupViewModel
    {
        public Guid TrainerGroupId { get; set; }
        public string TrainerGroupName { get; set; }

        public ICollection<TrainerViewModel> Trainers { get; set; }
        public TrainerGroupViewModel()
        {
            Trainers = new List<TrainerViewModel>();
        }
    }
}