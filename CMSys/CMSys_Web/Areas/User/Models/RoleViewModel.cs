﻿using System;
using System.Collections.Generic;

namespace CMSys_Web.Areas.User.Models
{
    public class RoleViewModel
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }

        public ICollection<UserViewModel> Users { get; set; }
        public RoleViewModel()
        {
            Users = new List<UserViewModel>();
        }
    }
}