﻿using System;
using System.Collections.Generic;

namespace CMSys_Web.Areas.User.Models
{
    public class CourseViewModel
    {
        public Guid CourseId { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }
        public bool IsNewCourse { get; set; }
        public int CodeType { get; set; }
        public int CodePlanningMethod { get; set; }

        public Guid CourseGroupId { get; set; }
        public CourseGroupViewModel CourseGroup { get; set; }

        public ICollection<TrainerViewModel> Trainers { get; set; }

        public CourseViewModel()
        {
            Trainers = new List<TrainerViewModel>();
        }
    }
}