﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMSys_Web.Areas.User.Models
{
    public class UserViewModel
    {
        public Guid UserId { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public string PasswordHash { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public string PasswordSalt { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public string Firstname { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public string Lastname { get; set; }
        [Required(ErrorMessage = "Field must be set")]
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Department { get; set; }
        public string OfficeLocation { get; set; }
        public string Position { get; set; }
        public byte[] Photo { get; set; }
        [NotMapped]
        public string FullName => Firstname + " " + Lastname;

        public ICollection<RoleViewModel> Roles { get; set; }
        public UserViewModel()
        {
            Roles = new List<RoleViewModel>();
        }
    }
}