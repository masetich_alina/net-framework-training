﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Handlers.Mappings;

namespace CMSys_DAL_Handlers.EF
{
    public class CMSysContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseGroup> CourseGroups { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Trainer> Trainers { get; set; }
        public DbSet<TrainerGroup> TrainerGroups { get; set; }
        public DbSet<User> Users { get; set; }

        public CMSysContext() : base("name=CMSys")
        {
        }

        public CMSysContext(DbCompiledModel model) 
            : base("name=CMSys", model)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CourseMap());
            modelBuilder.Configurations.Add(new CourseGroupMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new TrainerGroupMap());
            modelBuilder.Configurations.Add(new TrainerMap());
            modelBuilder.Configurations.Add(new UserMap());
        }
    }
}
