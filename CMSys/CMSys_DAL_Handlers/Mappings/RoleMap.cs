﻿using System.Data.Entity.ModelConfiguration;
using CMSys_DAL_Entities.Entities;

namespace CMSys_DAL_Handlers.Mappings
{
    public class RoleMap : EntityTypeConfiguration<Role>
    {
        public RoleMap()
        {
            ToTable("Role");

            HasKey(x => x.RoleId);

            Property(x => x.RoleName).IsRequired().HasMaxLength(32);

            HasMany(role => role.Users)
                .WithMany(user => user.Roles)
                .Map(m => m.ToTable("User_Role_Xref").MapLeftKey("roleId").MapRightKey("userId"));
        }
    }
}
