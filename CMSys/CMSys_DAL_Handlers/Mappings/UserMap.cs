﻿using System.Data.Entity.ModelConfiguration;
using CMSys_DAL_Entities.Entities;

namespace CMSys_DAL_Handlers.Mappings
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            ToTable("User");

            HasKey(x => x.UserId);

            Property(x => x.Email).IsRequired().HasMaxLength(128);
            Property(x => x.PasswordHash).IsRequired().HasMaxLength(128);
            Property(x => x.PasswordSalt).IsRequired().HasMaxLength(128);
            Property(x => x.Firstname).IsRequired().HasMaxLength(128);
            Property(x => x.Lastname).IsRequired().HasMaxLength(128);
            Property(x => x.StartDate).IsRequired();
            Property(x => x.EndDate).IsOptional();
            Property(x => x.Department).IsOptional().HasMaxLength(128);
            Property(x => x.OfficeLocation).IsOptional().HasMaxLength(128);
            Property(x => x.Position).IsOptional().HasMaxLength(128);
            Property(x => x.Photo).IsOptional();

            HasMany(user => user.Roles)
                .WithMany(role => role.Users)
                .Map(m => m.ToTable("User_Role_Xref").MapLeftKey("userId").MapRightKey("roleId"));
        }
    }
}
