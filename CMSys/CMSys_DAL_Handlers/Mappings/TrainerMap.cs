﻿using System.Data.Entity.ModelConfiguration;
using CMSys_DAL_Entities.Entities;

namespace CMSys_DAL_Handlers.Mappings
{
    public class TrainerMap : EntityTypeConfiguration<Trainer>
    {
        public TrainerMap()
        {
            ToTable("Trainer");

            HasKey(x => x.TrainerId);

            Property(x => x.TrainerInfo).IsOptional().HasMaxLength(4000);

            HasRequired(x => x.User);

            HasMany(trainer => trainer.Courses)
                .WithMany(course => course.Trainers)
                .Map(m => m.ToTable("Trainer_Course_Xref").MapLeftKey("trainerId").MapRightKey("courseId"));
        }
    }

}