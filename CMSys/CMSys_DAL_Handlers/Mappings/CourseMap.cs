﻿using System.Data.Entity.ModelConfiguration;
using CMSys_DAL_Entities.Entities;

namespace CMSys_DAL_Handlers.Mappings
{
    public class CourseMap : EntityTypeConfiguration<Course>
    {
        public CourseMap()
        {
            ToTable("Course");

            HasKey(x => x.CourseId);

            Property(x => x.CourseName).IsRequired().HasMaxLength(64);
            Property(x => x.Description).IsOptional().HasMaxLength(4000);
            Property(x => x.IsNewCourse).IsRequired();
            Property(x => x.CodeType).IsRequired();

            HasRequired(x => x.CourseGroup);

            HasMany(course => course.Trainers)
                .WithMany(trainer => trainer.Courses)
                .Map(m => m.ToTable("Trainer_Course_Xref").MapLeftKey("courseId").MapRightKey("trainerId"));
        }
    }
}
