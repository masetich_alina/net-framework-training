﻿using System.Data.Entity.ModelConfiguration;
using CMSys_DAL_Entities.Entities;

namespace CMSys_DAL_Handlers.Mappings
{
    public class TrainerGroupMap : EntityTypeConfiguration<TrainerGroup>
    {
        public TrainerGroupMap()
        {
            ToTable("TrainerGroup");

            HasKey(x => x.TrainerGroupId);

            Property(x => x.TrainerGroupName).IsRequired().HasMaxLength(32);

            HasMany(trainerGroup => trainerGroup.Trainers)
                .WithRequired(trainer => trainer.TrainerGroup);
        }
    }
}
