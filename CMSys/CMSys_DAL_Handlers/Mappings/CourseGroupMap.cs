﻿using System.Data.Entity.ModelConfiguration;
using CMSys_DAL_Entities.Entities;

namespace CMSys_DAL_Handlers.Mappings
{
    public class CourseGroupMap : EntityTypeConfiguration<CourseGroup>
    {
        public CourseGroupMap()
        {
            ToTable("CourseGroup");

            HasKey(x => x.CourseGroupId);

            Property(x => x.CourseGroupName).IsRequired().HasMaxLength(32);

            HasMany(courseGroup => courseGroup.Courses) 
                .WithRequired(course => course.CourseGroup);
        }
    }
}
