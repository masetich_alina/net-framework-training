﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;
using CMSys_DAL_Handlers.EF;

namespace CMSys_DAL_Handlers.Repositories
{
    public class CourseGroupRepository : IRepository<CourseGroup>
    {
        private CMSysContext _db; 

        public CourseGroupRepository(CMSysContext context)
        {
            _db = context;
        }

        public IEnumerable<CourseGroup> GetAll()
        {
            return _db.CourseGroups;
        }

        public CourseGroup Get(Guid? id)
        {
            return _db.CourseGroups.Find(id);
        }

        public IEnumerable<CourseGroup> Find(Func<CourseGroup, bool> predicate)
        {
            return _db.CourseGroups.Where(predicate).ToList();
        }

        public void Create(CourseGroup item)
        {
            _db.CourseGroups.Add(item);
        }

        public void Update(CourseGroup item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(Guid id)
        {
            var courseGroup = _db.CourseGroups.Find(id);
            if (courseGroup != null)
                _db.CourseGroups.Remove(courseGroup);
        }
    }
}
