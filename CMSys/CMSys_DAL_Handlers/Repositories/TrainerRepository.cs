﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;
using CMSys_DAL_Handlers.EF;

namespace CMSys_DAL_Handlers.Repositories
{
    public class TrainerRepository : IRepository<Trainer>
    {
        private CMSysContext _db;

        public TrainerRepository(CMSysContext context)
        {
            _db = context;
        }

        public IEnumerable<Trainer> GetAll()
        {
            return _db.Trainers;
        }

        public Trainer Get(Guid? id)
        {
            return _db.Trainers.Find(id);
        }

        public IEnumerable<Trainer> Find(Func<Trainer, bool> predicate)
        {
            return _db.Trainers.Where(predicate).ToList();
        }

        public void Create(Trainer item)
        {
            _db.Trainers.Add(item);
        }

        public void Update(Trainer item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(Guid id)
        {
            var trainer = _db.Trainers.Find(id);
            if (trainer != null)
                _db.Trainers.Remove(trainer);
        }
    }
}
