﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;
using CMSys_DAL_Handlers.EF;

namespace CMSys_DAL_Handlers.Repositories
{
    public class CourseRepository : IRepository<Course>
    {
        private CMSysContext _db;

        public CourseRepository(CMSysContext context)
        {
            _db = context;
        }

        public IEnumerable<Course> GetAll()
        {
            return _db.Courses;
        }

        public Course Get(Guid? id)
        {
            return _db.Courses.Find(id);
        }

        public IEnumerable<Course> Find(Func<Course, bool> predicate)
        {
            return _db.Courses.Where(predicate).ToList();
        }

        public void Create(Course item)
        {
            _db.Courses.Add(item);
        }

        public void Update(Course item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(Guid id)
        {
            var course = _db.Courses.Find(id);
            if (course != null)
                _db.Courses.Remove(course);
        }
    }
}
