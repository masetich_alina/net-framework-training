﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;
using CMSys_DAL_Handlers.EF;

namespace CMSys_DAL_Handlers.Repositories
{
    public class TrainerGroupRepository : IRepository<TrainerGroup>
    {
        private CMSysContext _db;

        public TrainerGroupRepository(CMSysContext context)
        {
            _db = context;
        }

        public IEnumerable<TrainerGroup> GetAll()
        {
            return _db.TrainerGroups;
        }

        public TrainerGroup Get(Guid? id)
        {
            return _db.TrainerGroups.Find(id);
        }

        public IEnumerable<TrainerGroup> Find(Func<TrainerGroup, bool> predicate)
        {
            return _db.TrainerGroups.Where(predicate).ToList();
        }

        public void Create(TrainerGroup item)
        {
            _db.TrainerGroups.Add(item);
        }

        public void Update(TrainerGroup item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(Guid id)
        {
            var trainerGroup = _db.TrainerGroups.Find(id);
            if (trainerGroup != null)
                _db.TrainerGroups.Remove(trainerGroup);
        }
    }
}
