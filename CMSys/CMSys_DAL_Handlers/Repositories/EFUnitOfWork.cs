﻿using System;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;
using CMSys_DAL_Handlers.EF;

namespace CMSys_DAL_Handlers.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private CourseGroupRepository _courseGroupRepository;
        private CourseRepository _courseRepository;
        private RoleRepository _roleRepository;
        private TrainerGroupRepository _trainerGroupRepository;
        private TrainerRepository _trainerRepository;
        private UserRepository _userRepository;
        private readonly CMSysContext db;
        private bool _disposed;

        public EFUnitOfWork()
        {
            db = new CMSysContext();
        }

        public IRepository<CourseGroup> CourseGroups =>
            _courseGroupRepository ?? (_courseGroupRepository = new CourseGroupRepository(db));

        public IRepository<Course> Courses => _courseRepository ?? (_courseRepository = new CourseRepository(db));
        public IRepository<Role> Roles => _roleRepository ?? (_roleRepository = new RoleRepository(db));

        public IRepository<TrainerGroup> TrainerGroups =>
            _trainerGroupRepository ?? (_trainerGroupRepository = new TrainerGroupRepository(db));

        public IRepository<Trainer> Trainers => _trainerRepository ?? (_trainerRepository = new TrainerRepository(db));
        public IRepository<User> Users => _userRepository ?? (_userRepository = new UserRepository(db));

        public void Save()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing) db.Dispose();
            _disposed = true;
        }
    }
}