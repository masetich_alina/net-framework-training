﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;
using CMSys_DAL_Handlers.EF;

namespace CMSys_DAL_Handlers.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private CMSysContext _db;

        public UserRepository(CMSysContext context)
        {
            _db = context;
        }

        public IEnumerable<User> GetAll()
        {
            return _db.Users;
        }

        public User Get(Guid? id)
        {
            return _db.Users.Find(id);
        }

        public IEnumerable<User> Find(Func<User, bool> predicate)
        {
            return _db.Users.Where(predicate).ToList();
        }

        public void Create(User item)
        {
            _db.Users.Add(item);
        }

        public void Update(User item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(Guid id)
        {
            var user = _db.Users.Find(id);
            if (user != null)
                _db.Users.Remove(user);
        }
    }
}
