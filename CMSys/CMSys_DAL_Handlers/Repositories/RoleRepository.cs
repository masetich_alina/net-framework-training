﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;
using CMSys_DAL_Handlers.EF;

namespace CMSys_DAL_Handlers.Repositories
{
    public class RoleRepository : IRepository<Role>
    {
        private CMSysContext _db;

        public RoleRepository(CMSysContext context)
        {
            _db = context;
        }

        public IEnumerable<Role> GetAll()
        {
            return _db.Roles;
        }

        public Role Get(Guid? id)
        {
            return _db.Roles.Find(id);
        }

        public IEnumerable<Role> Find(Func<Role, bool> predicate)
        {
            return _db.Roles.Where(predicate).ToList();
        }

        public void Create(Role item)
        {
            _db.Roles.Add(item);
        }

        public void Update(Role item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(Guid id)
        {
            var role = _db.Roles.Find(id);
            if (role != null)
                _db.Roles.Remove(role);
        }
    }
}
