use CMSys
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Trainer__User'
)
	alter table dbo.Trainer
	drop constraint fk_Trainer__User
go

alter table dbo.Trainer
	add constraint fk_Trainer__User
	foreign key (userId) references dbo.[User](userId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Trainer__TrainerGroup'
)
	alter table dbo.Trainer
	drop constraint fk_Trainer__TrainerGroup
go

alter table dbo.Trainer
	add constraint fk_Trainer__TrainerGroup
	foreign key (trainerGroupId) references dbo.TrainerGroup(trainerGroupId)
	on delete cascade
go