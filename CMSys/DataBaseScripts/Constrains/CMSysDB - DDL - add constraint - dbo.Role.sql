use CMSys
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Role__RoleName'
)
	alter table dbo.[Role]
	drop constraint ak_Role__RoleName
go

alter table dbo.[Role]
add constraint ak_Role__RoleName unique (roleName)
go
