use CMSys
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Trainer_Course_Xref__Trainer'
)
	alter table dbo.Trainer_Course_Xref
	drop constraint fk_Trainer_Course_Xref__Trainer
go

alter table dbo.Trainer_Course_Xref
	add constraint fk_Trainer_Course_Xref__Trainer
	foreign key (trainerId) references dbo.Trainer(trainerId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Trainer_Course_Xref__Course'
)
	alter table dbo.Trainer_Course_Xref
	drop constraint fk_Trainer_Course_Xref__Course
go

alter table dbo.Trainer_Course_Xref
	add constraint fk_Trainer_Course_Xref__Course
	foreign key (courseId) references dbo.Course(courseId)
	on delete cascade
go