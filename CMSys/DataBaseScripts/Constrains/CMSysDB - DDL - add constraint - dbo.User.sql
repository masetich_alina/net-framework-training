use CMSys
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_User__Email'
)
	alter table dbo.[User]
	drop constraint ak_User__Email
go

alter table dbo.[User]
add constraint ak_User__Email unique (email)
go
