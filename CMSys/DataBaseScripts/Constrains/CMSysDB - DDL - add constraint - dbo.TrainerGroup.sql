use CMSys
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_TrainerGroup__TrainerGroupName'
)
	alter table dbo.TrainerGroup
	drop constraint ak_TrainerGroup__TrainerGroupName
go

alter table dbo.TrainerGroup
add constraint ak_TrainerGroup__TrainerGroupName unique (trainerGroupName)
go
