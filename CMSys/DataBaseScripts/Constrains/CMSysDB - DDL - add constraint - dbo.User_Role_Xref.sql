use CMSys
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_User_Role_Xref__User'
)
	alter table dbo.User_Role_Xref
	drop constraint fk_User_Role_Xref__User
go

alter table dbo.User_Role_Xref
	add constraint fk_User_Role_Xref__User
	foreign key (userId) references dbo.[User](userId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_User_Role_Xref__Role'
)
	alter table dbo.User_Role_Xref
	drop constraint fk_User_Role_Xref__Role
go

alter table dbo.User_Role_Xref
	add constraint fk_User_Role_Xref__Role
	foreign key (roleId) references dbo.[Role](roleId)
	on delete cascade
go