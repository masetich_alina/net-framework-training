use CMSys
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_CourseGroup__CourseGroupName'
)
	alter table dbo.CourseGroup
	drop constraint ak_CourseGroup__CourseGroupName
go

alter table dbo.CourseGroup
add constraint ak_CourseGroup__CourseGroupName unique (courseGroupName)
go
