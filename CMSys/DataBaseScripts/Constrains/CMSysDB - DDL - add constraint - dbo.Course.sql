use CMSys
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Course__CourseGroup'
)
	alter table dbo.Course
	drop constraint fk_Course__CourseGroup
go

alter table dbo.Course
	add constraint fk_Course__CourseGroup
	foreign key (courseGroupId) references dbo.CourseGroup(courseGroupId)
	on delete no action
go