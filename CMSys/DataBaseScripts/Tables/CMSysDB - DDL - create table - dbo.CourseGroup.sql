use CMSys
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'CourseGroup'
)
	drop table dbo.CourseGroup
	go

create table dbo.CourseGroup
(
	courseGroupId	   	   uniqueIdentifier      not null,
	courseGroupName        nvarchar(32)          not null,
        		
	constraint pk_CourseGroup primary key (courseGroupId)
)
go