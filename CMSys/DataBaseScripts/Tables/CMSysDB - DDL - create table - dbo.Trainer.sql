use CMSys
go

if exists
(
	select 1 
	from sys.Tables
where Name = 'Trainer'
)
	drop table dbo.Trainer
	go

create table dbo.Trainer
(
	trainerId	   	    uniqueIdentifier     not null,
	trainerInfo         nvarchar(4000),
    userId              uniqueIdentifier     not null,
    trainerGroupId      uniqueIdentifier     not null,
    		
	constraint pk_Trainer primary key (trainerId)
)
go