use CMSys
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Trainer_Course_Xref'
)
	drop table dbo.Trainer_Course_Xref
	go

create table dbo.Trainer_Course_Xref
(
	trainerId        uniqueIdentifier        not null,
	courseId         uniqueIdentifier        not null,
        		
    constraint pk_Trainer_Course_Xref primary key (trainerId, courseId)
)
go