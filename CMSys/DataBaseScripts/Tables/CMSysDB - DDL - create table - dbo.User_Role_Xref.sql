use CMSys
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'User_Role_Xref'
)
	drop table dbo.User_Role_Xref
	go

create table dbo.User_Role_Xref
(
	userId        uniqueIdentifier        not null,
	roleId        uniqueIdentifier        not null,
        		
    constraint pk_User_Role_Xref primary key (userId, roleId)
)
go