use CMSys
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'User'
)
	drop table dbo.[User]
	go

create table dbo.[User]
(
	userId	    	    uniqueIdentifier    not null,
	email               nvarchar(128)       not null,
    passwordHash        nvarchar(128)       not null,
    passwordSalt        nvarchar(128)       not null,
	firstname           nvarchar(128)       not null,
    lastname            nvarchar(128)       not null,
    startDate           date                not null,
    endDate             date,
    department          nvarchar(128),
    officeLocation      nvarchar(128),
    position            nvarchar(128),
    photo               varbinary(max), 
    		
	constraint pk_User primary key (userId)
)
go