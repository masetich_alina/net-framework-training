use CMSys
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Course'
)
	drop table dbo.Course
	go

create table dbo.Course
(
	courseId	   	            uniqueIdentifier      not null,
	courseName                  nvarchar(64)          not null,
    [description]               nvarchar(4000),
    IsNewCourse                 bit                   not null,
    codeType                    int                   not null,
    codePlanningMethod          int                   not null, 
    courseGroupId               uniqueIdentifier      not null
    		
	constraint pk_Course primary key (courseId)
)
go