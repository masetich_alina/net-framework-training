use CMSys
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Role'
)
	drop table dbo.[Role]
	go

create table dbo.[Role]
(
	roleId	   	    uniqueIdentifier        not null,
	roleName        nvarchar(32)            not null,
    		
	constraint pk_Role primary key (roleId)
)
go