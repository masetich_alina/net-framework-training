use CMSys
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'TrainerGroup'
)
	drop table dbo.TrainerGroup
	go

create table dbo.TrainerGroup
(
	trainerGroupId	   	    uniqueidentifier      not null,
	trainerGroupName        nvarchar(32)          not null,
    		
	constraint pk_TrainerGroup primary key (trainerGroupId)
)
go