﻿using System;
using System.Collections.Generic;
using CMSys_DAL_Entities.Entities;

namespace CMSys_DAL_Entities.Interfaces
{
    public interface IRepository<T> where T : Entity
    {
        IEnumerable<T> GetAll();
        T Get(Guid? id);
        IEnumerable<T> Find(Func<T, bool> predicate);
        void Create(T item);
        void Update(T item);
        void Delete(Guid id);
    }
}
