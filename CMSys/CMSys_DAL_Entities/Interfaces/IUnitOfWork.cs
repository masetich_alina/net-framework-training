﻿using System;
using CMSys_DAL_Entities.Entities;

namespace CMSys_DAL_Entities.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Course> Courses { get; }
        IRepository<CourseGroup> CourseGroups { get; }
        IRepository<Role> Roles { get; }
        IRepository<Trainer> Trainers { get; }
        IRepository<TrainerGroup> TrainerGroups { get; }
        IRepository<User> Users { get; }
        void Save();
    }
}