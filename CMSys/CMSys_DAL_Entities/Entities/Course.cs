﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMSys_DAL_Entities.Entities
{
    public class Course : Entity
    {
        public Guid CourseId { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }
        public bool IsNewCourse { get; set; }
        public int CodeType { get; set; }
        public int CodePlanningMethod { get; set; }

        public Guid CourseGroupId { get; set; }
        public CourseGroup CourseGroup { get; set; }

        [NotMapped]
        public string SelectedTrainerId { get; set; }

        public virtual ICollection<Trainer> Trainers { get; set; }

        public Course()
        {
            Trainers = new List<Trainer>();
        }

        public override string ToString()
        {
            return $"{CourseId} - {CourseName}";
        }
    }
}