﻿using System;
using System.Collections.Generic;

namespace CMSys_DAL_Entities.Entities
{
    public class Trainer : Entity
    {
        public Guid TrainerId { get; set; }
        public string TrainerInfo { get; set; }

        public Guid UserId { get; set; }
        public virtual User User { get; set; }

        public Guid TrainerGroupId { get; set; }
        public virtual TrainerGroup TrainerGroup { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public Trainer()
        {
            Courses = new List<Course>();
        }

        public override string ToString()
        {
            return $"{TrainerId} - {TrainerInfo}";
        }
    }
}