﻿using System;
using System.Collections.Generic;

namespace CMSys_DAL_Entities.Entities
{
    public class CourseGroup : Entity
    {
        public Guid CourseGroupId { get; set; }
        public string CourseGroupName { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public CourseGroup()
        {
            Courses = new List<Course>();
        }

        public override string ToString()
        {
            return $"{CourseGroupId} - {CourseGroupName}";
        }
    }
}