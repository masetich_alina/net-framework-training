﻿using System;
using System.Collections.Generic;

namespace CMSys_DAL_Entities.Entities
{
    public class Role : Entity
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public Role()
        {
            Users = new List<User>();
        }

        public override string ToString()
        {
            return $"{RoleId} - {RoleName}";
        }
    }
}