﻿using System;
using System.Collections.Generic;

namespace CMSys_DAL_Entities.Entities
{
    public class TrainerGroup : Entity
    {
        public Guid TrainerGroupId { get; set; }
        public string TrainerGroupName { get; set; }

        public virtual ICollection<Trainer> Trainers { get; set; }

        public TrainerGroup()
        {
            Trainers = new List<Trainer>();
        }

        public override string ToString()
        {
            return $"{TrainerGroupId} - {TrainerGroupName}";
        }
    }
}