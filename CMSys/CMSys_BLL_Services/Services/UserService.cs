﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;
using static System.String;

namespace CMSys_BLL_Services.Services
{
    public class UserService : IUserService
    {
        private IUnitOfWork Database { get; set; }

        public UserService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public void CreateUser(UserDto userDto)
        {
            var user = new User
            {
                UserId = userDto.UserId,
                Email = userDto.Email,
                PasswordHash = userDto.PasswordHash,
                PasswordSalt = userDto.PasswordSalt,
                Firstname = userDto.Firstname,
                Lastname = userDto.Lastname,
                StartDate = userDto.StartDate,
                EndDate = userDto.EndDate,
                Department = userDto.Department,
                OfficeLocation = userDto.OfficeLocation,
                Position = userDto.Position,
                Photo = userDto.Photo
                
            };
            Database.Users.Create(user);
            Database.Save();
        }

        public void EditUser(UserDto userDto)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, User>()).CreateMapper();
            var user = mapper.Map<UserDto, User>(userDto);
            Database.Users.Update(user);
            Database.Save();
        }
        
        public void DeleteUser(Guid id)
        {
            var user = GetUser(id);
            if (user != null && user.Roles.Count > 0)
            {
                throw new ValidationException("User has dependents and cannot be deleted", "");
            }
            Database.Users.Delete(id);
            Database.Save();
        }

        public void DeleteRole(Guid userId, Guid roleId)
        {
            var user = Database.Users.Find(x => x.UserId == userId).AsQueryable().FirstOrDefault();
            var role = Database.Roles.Find(x => x.RoleId == roleId).AsQueryable().FirstOrDefault();
            if (user == null)
            {
                throw new ValidationException("User is not found", "");
            }
            if (role == null)
            {
                throw new ValidationException("Role is not found", "");
            }
            user.Roles.Remove(role);
            Database.Users.Update(user);
            Database.Save();
        }

        public void AddRole(Guid userId, Guid roleId)
        {
            var user = Database.Users.Find(x => x.UserId == userId).AsQueryable().FirstOrDefault();
            var role = Database.Roles.Find(x => x.RoleId == roleId).AsQueryable().FirstOrDefault();
            if (user == null)
            {
                throw new ValidationException("User is not found", "");
            }
            if (role == null)
            {
                throw new ValidationException("Role is not found", "");
            }
            user.Roles.Add(role);
            Database.Users.Update(user);
            Database.Save();
        }

        public UserDto GetUser(Guid? userId)
        {
            if (userId == null)
                throw new ValidationException("Could not find id", "");
            var user = Database.Users.Get(userId);
            if (user == null)
                throw new ValidationException("User is not found", "");

            var userDto =  new UserDto
            {
                UserId = user.UserId,
                Email = user.Email,
                PasswordHash = user.PasswordHash,
                PasswordSalt = user.PasswordSalt,
                Firstname = user.Firstname,
                Lastname = user.Lastname,
                StartDate = user.StartDate,
                EndDate = user.EndDate,
                Department = user.Department,
                OfficeLocation = user.OfficeLocation,
                Position = user.Position,
                Photo = user.Photo
            };
            foreach (var role in user.Roles)
            {
                userDto.Roles.Add(new RoleDto
                {
                   RoleId = role.RoleId,
                   RoleName = role.RoleName
                });
            }

            return userDto;
        }

        public IEnumerable<UserDto> GetUsersByFullname(string fullname)
        {
            var users = Database.Users.Find(x => x.FullName.Contains(fullname));
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDto>()).CreateMapper();
            return mapper.Map<IEnumerable<User>, List<UserDto>>(users);
        }

        public IEnumerable<UserDto> GetUsers()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDto>()).CreateMapper();
            return mapper.Map<IEnumerable<User>, List<UserDto>>(Database.Users.GetAll());
        }

        public string HashPassword(string password, string salt)
        {
            var combinedPassword = Concat(password, salt);
            var sha256 = new SHA256Managed();
            var bytes = Encoding.UTF8.GetBytes(combinedPassword);
            var hash = sha256.ComputeHash(bytes);
            return Convert.ToBase64String(hash);
        }

        public bool ValidatePassword(string enteredPassword, string storedHash, string storedSalt)
        {
            var hash = HashPassword(enteredPassword, storedSalt);
            return string.Equals(storedHash, hash);
        }

        public UserDto Login(string email, string password)
        {
            var user = Database.Users.Find(p => Compare(p.Email, email, StringComparison.OrdinalIgnoreCase) == 0).FirstOrDefault();
            if (user == null)
            {
                throw new ValidationException("User is not found", "");
            }
            if (!ValidatePassword(password, user.PasswordHash, user.PasswordSalt))
            {
                throw new ValidationException("Wrong password", "");
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDto>()).CreateMapper();
            var userDto = mapper.Map<User, UserDto>(user);
            return userDto;
        }

        public UserDto GetUserByEmail(string email)
        {
            var user = Database.Users.Find(x => x.Email == email).AsQueryable().FirstOrDefault();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDto>()).CreateMapper();
            return mapper.Map<User, UserDto>(user);
        }

        public string[] GetRolesForUser(string username)
        {
            var roles = new List<string>();

            var user = GetUserByEmail(username);
            if (user?.Roles != null)
            {
                roles = user.Roles.Select(x => x.RoleName).ToList();
            }

            return roles.ToArray();
        }

        public bool IsUserInRole(string username, string roleName)
        {
            var user = GetUserByEmail(username);
            return user?.Roles != null && user.Roles.Any(role => role.RoleName == roleName);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
