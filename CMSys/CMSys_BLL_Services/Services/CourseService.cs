﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;

namespace CMSys_BLL_Services.Services
{
    public class CourseService : ICourseService
    {
        private IUnitOfWork Database { get; set; }

        public CourseService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public void CreateCourse(CourseDto courseDto)
        {
            if (courseDto.CourseName == null)
            {
                throw new ValidationException("", "");
            }
            var course = new Course
            {
                CourseId = courseDto.CourseId,
                CourseName = courseDto.CourseName,
                Description = courseDto.Description,
                IsNewCourse = courseDto.IsNewCourse,
                CodeType = courseDto.CodeType,
                CodePlanningMethod = courseDto.CodePlanningMethod,
                CourseGroupId = courseDto.CourseGroupId
            };
            Database.Courses.Create(course);
            Database.Save();
        }

        public void EditCourse(CourseDto courseDto)
        {
            if (courseDto.CourseName == null)
            {
                throw new ValidationException("", "");
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseDto, Course>()).CreateMapper();
            var course = mapper.Map<CourseDto, Course>(courseDto);
            Database.Courses.Update(course);
            Database.Save();
        }

        public void DeleteCourse(Guid id)
        {
            var course = GetCourse(id);
            if (course.Trainers.Count > 0 )
            {
                throw new ValidationException("Course has dependents and cannot be deleted", "");
            }
            Database.Courses.Delete(id);
            Database.Save();
        }

        public CourseDto GetCourse(Guid? courseId)
        {
            if (courseId == null)
            {
                throw new ValidationException("Could not find id", "");
            }
            var course = Database.Courses.Get(courseId);
            if (course == null)
            {
                throw new ValidationException("Course is not found", "");
            }

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Course, CourseDto>()).CreateMapper();
            return mapper.Map<Course, CourseDto>(course);
        }

        public void DeleteTrainerInCourse(Guid courseId, Guid trainerId)
        {
            var course = Database.Courses.Find(x => x.CourseId == courseId).AsQueryable().FirstOrDefault();
            var trainer = Database.Trainers.Find(x => x.TrainerId == trainerId).AsQueryable().FirstOrDefault();
            if (course == null)
            {
                throw new ValidationException("Course is not found", "");
            }
            course.Trainers.Remove(trainer);
            Database.Courses.Update(course);
            Database.Save();
        }

        public void AddTrainerInCourse(Guid courseId, Guid trainerId)
        {
            var course = Database.Courses.Find(x => x.CourseId == courseId).AsQueryable().FirstOrDefault();
            var trainer = Database.Trainers.Find(x => x.TrainerId == trainerId).AsQueryable().FirstOrDefault();
            if (course == null)
            {
                throw new ValidationException("Course is not found", "");
            }
            if (trainer == null)
            {
                throw new ValidationException("Trainer is not found", "");
            }
            course.Trainers.Add(trainer);
            Database.Courses.Update(course);
            Database.Save();
        }

        public IEnumerable<CourseDto> GetCourses()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Course, CourseDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Course>, List<CourseDto>>(Database.Courses.GetAll());
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
