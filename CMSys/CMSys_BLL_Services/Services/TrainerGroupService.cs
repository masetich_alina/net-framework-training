﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;

namespace CMSys_BLL_Services.Services
{
    public class TrainerGroupService : ITrainerGroupService
    {
        private IUnitOfWork Database { get; set; }

        public TrainerGroupService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public void CreateTrainerGroup(TrainerGroupDto trainerGroupDto)
        {
            if (trainerGroupDto.TrainerGroupName == null)
            {
                throw new ValidationException("", "");
            }
            var trainerGroup = new TrainerGroup()
            {
                TrainerGroupId = trainerGroupDto.TrainerGroupId,
                TrainerGroupName = trainerGroupDto.TrainerGroupName
            };
            Database.TrainerGroups.Create(trainerGroup);
            Database.Save();
        }

        public void EditTrainerGroup(TrainerGroupDto trainerGroupDto)
        {
            if (trainerGroupDto.TrainerGroupName == null)
            {
                throw new ValidationException("", "");
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerGroupDto, TrainerGroup>()).CreateMapper();
            var trainerGroup = mapper.Map<TrainerGroupDto, TrainerGroup>(trainerGroupDto);
            Database.TrainerGroups.Update(trainerGroup);
            Database.Save();
        }

        public void DeleteTrainerGroup(Guid id)
        {
            if (Database.Trainers.Find(x => x.TrainerGroupId == id).Any())
            {
                throw new ValidationException("Trainer Group has dependents and cannot be deleted", "");
            }
            Database.TrainerGroups.Delete(id);
            Database.Save();
        }

        public TrainerGroupDto GetTrainerGroup(Guid? trainerGroupId)
        {
            if (trainerGroupId == null)
                throw new ValidationException("Could not find id", "");
            var trainerGroup = Database.TrainerGroups.Get(trainerGroupId);
            if (trainerGroup == null)
                throw new ValidationException("Trainer Group is not found", "");

            return new TrainerGroupDto
            {
                TrainerGroupId = trainerGroup.TrainerGroupId,
                TrainerGroupName = trainerGroup.TrainerGroupName
            };
        }

        public IEnumerable<TrainerGroupDto> GetTrainerGroups()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerGroup, TrainerGroupDto>()).CreateMapper();
            return mapper.Map<IEnumerable<TrainerGroup>, List<TrainerGroupDto>>(Database.TrainerGroups.GetAll());
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
