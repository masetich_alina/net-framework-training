﻿using System;
using System.Collections.Generic;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;

namespace CMSys_BLL_Services.Services
{
    public class RoleService : IRoleService
    {
        private IUnitOfWork Database { get; set; }

        public RoleService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public void CreateRole(RoleDto roleDto)
        {
            if (roleDto.RoleName == null)
            {
                throw new ValidationException("", "");
            }
            var role = new Role
            {
                RoleId = roleDto.RoleId,
                RoleName = roleDto.RoleName
            };
            Database.Roles.Create(role);
            Database.Save();
        }

        public void EditRole(RoleDto roleDto)
        {
            if (roleDto.RoleName == null)
            {
                throw new ValidationException("", "");
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<RoleDto, Role>()).CreateMapper();
            var role = mapper.Map<RoleDto, Role>(roleDto);
            Database.Roles.Update(role);
            Database.Save();
        }

        public void DeleteRole(Guid id)
        {
            var role = GetRole(id);
            if (role.Users.Count > 0)
            {
                throw new ValidationException("Role has dependents and cannot be deleted", "");
            }
            Database.Roles.Delete(id);
            Database.Save();
        }

        public RoleDto GetRole(Guid? roleId)
        {
            if (roleId == null)
            {
                throw new ValidationException("Could not find id", "");
            }
            var role = Database.Roles.Get(roleId);
            if (role == null)
            {
                throw new ValidationException("Role is not found", "");
            }

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Role, RoleDto>()).CreateMapper();
            return mapper.Map<Role, RoleDto>(role);
        }

        public IEnumerable<RoleDto> GetRoles()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Role, RoleDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Role>, List<RoleDto>>(Database.Roles.GetAll());
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
