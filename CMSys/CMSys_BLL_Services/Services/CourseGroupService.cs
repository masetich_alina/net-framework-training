﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;

namespace CMSys_BLL_Services.Services
{
    public class CourseGroupService : ICourseGroupService
    {
        private IUnitOfWork Database { get; set; }

        public CourseGroupService(IUnitOfWork uow) 
        {
            Database = uow;
        }

        public void CreateCourseGroup(CourseGroupDto courseGroupDto)
        {
            if (courseGroupDto.CourseGroupName == null)
            {
                throw new ValidationException("", "");
            }
            var courseGroup = new CourseGroup()
            {
                CourseGroupId = courseGroupDto.CourseGroupId,
                CourseGroupName = courseGroupDto.CourseGroupName
            };
            Database.CourseGroups.Create(courseGroup);
            Database.Save();
        }

        public void EditCourseGroup(CourseGroupDto courseGroupDto)
        {
            if (courseGroupDto.CourseGroupName == null)
            {
                throw new ValidationException("", "");
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseGroupDto, CourseGroup>()).CreateMapper();
            var courseGroup = mapper.Map<CourseGroupDto, CourseGroup>(courseGroupDto);
            Database.CourseGroups.Update(courseGroup);
            Database.Save();
        }

        public void DeleteCourseGroup(Guid id)
        {
            if (Database.Courses.Find(x => x.CourseGroupId == id).Any())
            {
                throw new ValidationException("Course Group has dependents and cannot be deleted", "");
            }
            Database.CourseGroups.Delete(id);
            Database.Save();
        }

        public CourseGroupDto GetCourseGroup(Guid? courseGroupId)
        {
            if (courseGroupId == null)
                throw new ValidationException("Could not find id", "");
            var courseGroup = Database.CourseGroups.Get(courseGroupId);
            if (courseGroup == null)
                throw new ValidationException("Course Group is not found", "");

            return new CourseGroupDto
            {
                CourseGroupId = courseGroup.CourseGroupId,
                CourseGroupName = courseGroup.CourseGroupName
            };
        }

        public IEnumerable<CourseGroupDto> GetCourseGroups()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CourseGroup, CourseGroupDto>()).CreateMapper();
            return mapper.Map<IEnumerable<CourseGroup>, List<CourseGroupDto>>(Database.CourseGroups.GetAll());
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
