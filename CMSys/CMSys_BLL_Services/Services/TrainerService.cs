﻿using System;
using System.Collections.Generic;
using AutoMapper;
using CMSys_BLL_Intermediate_Entities.DTO;
using CMSys_BLL_Intermediate_Entities.Interfaces;
using CMSys_BLL_Services.Infrastructure;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Entities.Interfaces;

namespace CMSys_BLL_Services.Services
{
    public class TrainerService : ITrainerService
    {
        private IUnitOfWork Database { get; set; }

        public TrainerService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public void CreateTrainer(TrainerDto trainerDto)
        {
            var trainer = new Trainer
            {
                TrainerId = trainerDto.TrainerId,
                TrainerInfo = trainerDto.TrainerInfo,
                TrainerGroupId = trainerDto.TrainerGroupId,
                UserId = trainerDto.UserId
            };
            Database.Trainers.Create(trainer);
            Database.Save();
        }

        public void EditTrainer(TrainerDto trainerDto)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TrainerDto, Trainer>()).CreateMapper();
            var trainer = mapper.Map<TrainerDto, Trainer>(trainerDto);
            Database.Trainers.Update(trainer);
            Database.Save();
        }

        public void DeleteTrainer(Guid id)
        {
            var trainer = GetTrainer(id);
            if (trainer.Courses.Count > 0)
            {
                throw new ValidationException("Trainer has dependents and cannot be deleted", "");
            }
            Database.Trainers.Delete(id);
            Database.Save();
        }

        public TrainerDto GetTrainer(Guid? trainerId)
        {
            if (trainerId == null)
                throw new ValidationException("Could not find id", "");
            var trainer = Database.Trainers.Get(trainerId);
            if (trainer == null)
                throw new ValidationException("Trainer is not found", "");

            var trainerDto = new TrainerDto
            {
               TrainerId = trainer.TrainerId,
               TrainerInfo = trainer.TrainerInfo,
               UserId = trainer.UserId,
               TrainerGroupId = trainer.TrainerGroupId
            };
            foreach (var course in trainer.Courses)
            {
                trainerDto.Courses.Add(new CourseDto
                {
                    CourseId = course.CourseId,
                    CourseName = course.CourseName,
                    Description = course.Description,
                    IsNewCourse = course.IsNewCourse,
                    CodeType = course.CodeType,
                    CodePlanningMethod = course.CodePlanningMethod,
                    CourseGroupId = course.CourseGroupId
                });
            }

            return trainerDto;
        }

        public IEnumerable<TrainerDto> GetTrainers()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Trainer, TrainerDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Trainer>, List<TrainerDto>>(Database.Trainers.GetAll());
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
