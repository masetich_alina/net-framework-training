﻿using CMSys_DAL_Entities.Interfaces;
using CMSys_DAL_Handlers.Repositories;
using Ninject.Modules;

namespace CMSys_BLL_Services.Services
{
    public class ServiceModule : NinjectModule
    {
        private readonly string _connectionString;
        public ServiceModule(string connection)
        {
            _connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<EFUnitOfWork>().WithConstructorArgument(_connectionString);
        }
    }
}
