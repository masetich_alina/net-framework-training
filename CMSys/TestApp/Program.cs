﻿using System;
using System.Collections.Generic;
using CMSys_DAL_Entities.Entities;
using CMSys_DAL_Handlers.Repositories;

namespace TestApp
{
    public class Program
    {
        private static void OutputText(IEnumerable<Entity> list)
        {
            Console.WriteLine();
            foreach (var l in list)
            {
                Console.WriteLine(l.ToString());
            }
        }

        private static void Main(string[] args)
        {

            var db = new EFUnitOfWork();

            #region Roles
            Console.WriteLine("Roles:");

            //GetAll
            OutputText(db.Roles.GetAll());

            //Get
            var getRoleById = db.Roles.Get(Guid.Parse("59b4f489-9603-4db6-a20b-823b69892cff"));

            //Update
            getRoleById.RoleName = "Admin_";
            db.Roles.Update(getRoleById);
            db.Save();

            OutputText(db.Roles.GetAll());

            getRoleById.RoleName = "Admin";
            db.Roles.Update(getRoleById);
            db.Save();

            //Create
            var role = new Role
            {
                RoleId = Guid.NewGuid(),
                RoleName = "User"
            };
            db.Roles.Create(role);
            db.Save();

            OutputText(db.Roles.GetAll());

            //Delete
            db.Roles.Delete(role.RoleId);
            db.Save();

            OutputText(db.Roles.GetAll());


            #endregion

            #region Users

            Console.WriteLine("\nUsers:");

            //GetAll
            OutputText(db.Users.GetAll());

            //Get
            var getUserById = db.Users.Get(Guid.Parse("59b4f489-9603-4db6-a20b-823b69892caa"));

            //Create
            var user = new User
            {
                UserId = Guid.NewGuid(),
                Email = "HannaKaleinik@coherentsolutions.com",
                PasswordHash = "34567",
                PasswordSalt = "45673",
                Firstname = "Hanna",
                Lastname = "Kaleinik",
                StartDate = new DateTime(2016, 1, 4)
            };
            db.Users.Create(user);
            db.Save();

            OutputText(db.Users.GetAll());

            //Update
            getUserById.Firstname = "RRRoman";
            db.Users.Update(getUserById);
            db.Save();

            OutputText(db.Users.GetAll());

            getUserById.Firstname = "Roman";
            db.Users.Update(getUserById);
            db.Save();

            //Delete
            db.Users.Delete(user.UserId);
            db.Save();

            OutputText(db.Users.GetAll());

            #endregion

            #region CourseGroups

            Console.WriteLine("\nCourseGroups:");
            //GetAll
            OutputText(db.CourseGroups.GetAll());

            //Get
            var getCourseGroupById = db.CourseGroups.Get(Guid.Parse("59b4f489-9603-4db6-a20b-823b69892ccc"));

            //Update
            getCourseGroupById.CourseGroupName = "Net";
            db.CourseGroups.Update(getCourseGroupById);
            db.Save();

            OutputText(db.CourseGroups.GetAll());

            getCourseGroupById.CourseGroupName = ".NET Course Group";
            db.CourseGroups.Update(getCourseGroupById);
            db.Save();

            //Create
            var courseGroup = new CourseGroup
            {
                CourseGroupId = Guid.NewGuid(),
                CourseGroupName = "Java"
            };
            db.CourseGroups.Create(courseGroup);
            db.Save();

            OutputText(db.CourseGroups.GetAll());

            //Delete
            db.CourseGroups.Delete(courseGroup.CourseGroupId);
            db.Save();

            OutputText(db.CourseGroups.GetAll());

            #endregion

            #region Courses

            Console.WriteLine("\nCourses:");

            //GetAll
            OutputText(db.Courses.GetAll());

            //Get
            var getCourseById = db.Courses.Get(Guid.Parse("59b4f489-9603-4db6-a20b-823b69892acc"));

            //Update
            getCourseById.CourseName = "WPF";
            db.Courses.Update(getCourseById);
            db.Save();

            OutputText(db.Courses.GetAll());

            getCourseById.CourseName = "Windows Presentation Foundation";
            db.Courses.Update(getCourseById);
            db.Save();

            //Create
            var course = new Course
            {
                CourseId = Guid.NewGuid(),
                CourseName = "ASP.NET Core",
                IsNewCourse = false,
                CodeType = 4,
                CodePlanningMethod = 1,
                CourseGroupId = Guid.Parse("59b4f489-9603-4db6-a20b-823b69892ccc")
            };
            db.Courses.Create(course);
            db.Save();

            OutputText(db.Courses.GetAll());

            //Delete
            db.Courses.Delete(course.CourseId);
            db.Save();

            OutputText(db.Courses.GetAll());

            #endregion

            #region TrainerGroups

            Console.WriteLine("\nTrainerGroups:");
            //GetAll
            OutputText(db.TrainerGroups.GetAll());

            //Get
            var getTrainerGroupById = db.TrainerGroups.Get(Guid.Parse("59b4f489-9603-4db6-a20b-823b69892aaa"));

            //Update
            getTrainerGroupById.TrainerGroupName = "Net";
            db.TrainerGroups.Update(getTrainerGroupById);
            db.Save();

            OutputText(db.TrainerGroups.GetAll());

            getTrainerGroupById.TrainerGroupName = ".NET";
            db.TrainerGroups.Update(getTrainerGroupById);
            db.Save();

            //Create
            var trainerGroup = new TrainerGroup
            {
                TrainerGroupId = Guid.NewGuid(),
                TrainerGroupName = "JAVA"
            };
            db.TrainerGroups.Create(trainerGroup);
            db.Save();

            OutputText(db.TrainerGroups.GetAll());

            //Delete
            db.TrainerGroups.Delete(trainerGroup.TrainerGroupId);
            db.Save();

            OutputText(db.TrainerGroups.GetAll());

            #endregion

            #region Trainers

            Console.WriteLine("\nTrainers:");

            //GetAll
            OutputText(db.Trainers.GetAll());

            //Get
            var getTrainerById = db.Trainers.Get(Guid.Parse("59b4f489-9603-4db6-a20b-823b69892cdd"));

            //Update
            getTrainerById.TrainerInfo = "Trainer";
            db.Trainers.Update(getTrainerById);
            db.Save();

            OutputText(db.Trainers.GetAll());

            getTrainerById.TrainerInfo = "";
            db.Trainers.Update(getTrainerById);
            db.Save();

            #endregion

        }
    }
}