﻿using System.ComponentModel.DataAnnotations;

namespace CMSys_BLL_Intermediate_Entities.Enums
{
    public enum CodeTypes
    {
        [Display(Name = "Lectures")]
        Lectures = 1,
        [Display(Name = "Practices")]
        Practices = 2
    }
}
