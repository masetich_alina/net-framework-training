﻿using System.ComponentModel.DataAnnotations;

namespace CMSys_BLL_Intermediate_Entities.Enums
{
    public enum CodePlanningMethods
    {
        [Display(Name = "Scheduled")]
        Scheduled = 1,
        [Display(Name = "On demand")]
        OnDemand = 2
    }
}
