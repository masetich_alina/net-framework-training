﻿using System;
using System.Collections.Generic;

namespace CMSys_BLL_Intermediate_Entities.DTO
{
    public class CourseGroupDto
    {
        public Guid CourseGroupId { get; set; }
        public string CourseGroupName { get; set; }

        public ICollection<CourseDto> Courses { get; set; }

        public CourseGroupDto()
        {
            Courses = new List<CourseDto>();
        }

    }
}