﻿using System;
using System.Collections.Generic;

namespace CMSys_BLL_Intermediate_Entities.DTO
{
    public class TrainerGroupDto
    {
        public Guid TrainerGroupId { get; set; }
        public string TrainerGroupName { get; set; }

        public ICollection<TrainerDto> Trainers { get; set; }

        public TrainerGroupDto()
        {
            Trainers = new List<TrainerDto>();
        }
    }
}
