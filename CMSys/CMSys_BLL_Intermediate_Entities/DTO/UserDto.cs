﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMSys_BLL_Intermediate_Entities.DTO
{
    public class UserDto
    {
        public Guid UserId { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Department { get; set; }
        public string OfficeLocation { get; set; }
        public string Position { get; set; }
        public byte[] Photo { get; set; }
        [NotMapped]
        public string FullName => Firstname + " " + Lastname;
        [NotMapped]
        public string SelectedRoleId { get; set; }

        public ICollection<RoleDto> Roles { get; set; }

        public UserDto()
        {
            Roles = new List<RoleDto>();
        }

    }
}