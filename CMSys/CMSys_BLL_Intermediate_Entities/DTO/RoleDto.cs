﻿using System;
using System.Collections.Generic;

namespace CMSys_BLL_Intermediate_Entities.DTO
{
    public class RoleDto
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }

        public ICollection<UserDto> Users { get; set; }

        public RoleDto()
        {
            Users = new List<UserDto>();
        }
    }
}
