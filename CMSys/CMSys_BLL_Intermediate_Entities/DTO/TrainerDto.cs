﻿using System;
using System.Collections.Generic;

namespace CMSys_BLL_Intermediate_Entities.DTO
{
    public class TrainerDto
    {
        public Guid TrainerId { get; set; }
        public string TrainerInfo { get; set; }

        public Guid UserId { get; set; }
        public UserDto User { get; set; }

        public Guid TrainerGroupId { get; set; }
        public TrainerGroupDto TrainerGroup { get; set; }

        public ICollection<CourseDto> Courses { get; set; }

        public TrainerDto()
        {
            Courses = new List<CourseDto>();
        }

    }
}