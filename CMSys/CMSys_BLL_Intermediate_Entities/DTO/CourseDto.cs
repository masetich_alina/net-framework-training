﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMSys_BLL_Intermediate_Entities.DTO
{
    public class CourseDto
    {
        public Guid CourseId { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }
        public bool IsNewCourse { get; set; }
        public int CodeType { get; set; }
        public int CodePlanningMethod { get; set; }

        public Guid CourseGroupId { get; set; }
        public CourseGroupDto CourseGroup { get; set; }

        [NotMapped]
        public string SelectedTrainerId { get; set; }

        public CourseDto()
        {
            Trainers = new List<TrainerDto>();
        }

        public ICollection<TrainerDto> Trainers { get; set; }
    }
}