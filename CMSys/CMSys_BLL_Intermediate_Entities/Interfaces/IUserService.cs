﻿using System;
using System.Collections.Generic;
using CMSys_BLL_Intermediate_Entities.DTO;

namespace CMSys_BLL_Intermediate_Entities.Interfaces
{
    public interface IUserService
    {
        void CreateUser(UserDto userDto);
        void EditUser(UserDto userDto);
        void DeleteUser(Guid id);
        UserDto GetUser(Guid? userId);
        IEnumerable<UserDto> GetUsers();
        IEnumerable<UserDto> GetUsersByFullname(string fullname);
        UserDto Login(string email, string password);
        UserDto GetUserByEmail(string email);
        string[] GetRolesForUser(string username);
        bool IsUserInRole(string username, string roleName);
        void DeleteRole(Guid userId, Guid roleId);
        void AddRole(Guid userId, Guid roleId);
        void Dispose();
    }
}
