﻿using System;
using System.Collections.Generic;
using CMSys_BLL_Intermediate_Entities.DTO;

namespace CMSys_BLL_Intermediate_Entities.Interfaces
{
    public interface ITrainerService
    {
        void CreateTrainer(TrainerDto trainerDto);
        void EditTrainer(TrainerDto trainerDto);
        void DeleteTrainer(Guid id);
        TrainerDto GetTrainer(Guid? trainerId);
        IEnumerable<TrainerDto> GetTrainers();
        void Dispose();
    }
}
