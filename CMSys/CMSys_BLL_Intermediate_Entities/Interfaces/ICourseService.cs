﻿using System;
using System.Collections.Generic;
using CMSys_BLL_Intermediate_Entities.DTO;

namespace CMSys_BLL_Intermediate_Entities.Interfaces
{
    public interface ICourseService
    {
        void CreateCourse(CourseDto courseDto);
        void EditCourse(CourseDto courseDto);
        void DeleteCourse(Guid id);
        CourseDto GetCourse(Guid? courseId);
        void DeleteTrainerInCourse(Guid courseId, Guid trainerId);
        void AddTrainerInCourse(Guid courseId, Guid trainerId);
        IEnumerable<CourseDto> GetCourses();
        void Dispose();
    }
}
