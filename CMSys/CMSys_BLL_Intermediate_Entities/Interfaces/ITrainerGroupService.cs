﻿using System;
using System.Collections.Generic;
using CMSys_BLL_Intermediate_Entities.DTO;

namespace CMSys_BLL_Intermediate_Entities.Interfaces
{
    public interface ITrainerGroupService
    {
        void CreateTrainerGroup(TrainerGroupDto trainerGroupDto);
        void EditTrainerGroup(TrainerGroupDto trainerGroupDto);
        void DeleteTrainerGroup(Guid id);
        TrainerGroupDto GetTrainerGroup(Guid? trainerGroupId);
        IEnumerable<TrainerGroupDto> GetTrainerGroups();
        void Dispose(); 
    }
}
