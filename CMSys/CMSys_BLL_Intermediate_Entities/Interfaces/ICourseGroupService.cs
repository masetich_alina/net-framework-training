﻿using System;
using System.Collections.Generic;
using CMSys_BLL_Intermediate_Entities.DTO;

namespace CMSys_BLL_Intermediate_Entities.Interfaces
{
    public interface ICourseGroupService
    {
        void CreateCourseGroup(CourseGroupDto courseGroupDto);
        void EditCourseGroup(CourseGroupDto courseGroupDto);
        void DeleteCourseGroup(Guid id);
        CourseGroupDto GetCourseGroup(Guid? courseGroupId); 
        IEnumerable<CourseGroupDto> GetCourseGroups();
        void Dispose();
    }
}
