﻿using System;
using System.Collections.Generic;
using CMSys_BLL_Intermediate_Entities.DTO;

namespace CMSys_BLL_Intermediate_Entities.Interfaces
{
    public interface IRoleService
    {
        void CreateRole(RoleDto roleDto);
        void EditRole(RoleDto roleDto);
        void DeleteRole(Guid id);
        RoleDto GetRole(Guid? roleId);
        IEnumerable<RoleDto> GetRoles();
        void Dispose();
    }
}
