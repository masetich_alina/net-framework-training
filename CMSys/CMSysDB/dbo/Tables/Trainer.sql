﻿CREATE TABLE [dbo].[Trainer] (
    [trainerId]      UNIQUEIDENTIFIER NOT NULL,
    [trainerInfo]    NVARCHAR (4000)  NULL,
    [userId]         UNIQUEIDENTIFIER NOT NULL,
    [trainerGroupId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [pk_Trainer] PRIMARY KEY CLUSTERED ([trainerId] ASC),
    CONSTRAINT [fk_Trainer__TrainerGroup] FOREIGN KEY ([trainerGroupId]) REFERENCES [dbo].[TrainerGroup] ([trainerGroupId]) ON DELETE CASCADE,
    CONSTRAINT [fk_Trainer__User] FOREIGN KEY ([userId]) REFERENCES [dbo].[User] ([userId]) ON DELETE CASCADE
);

