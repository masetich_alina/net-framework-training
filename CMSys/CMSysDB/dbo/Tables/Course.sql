﻿CREATE TABLE [dbo].[Course] (
    [courseId]           UNIQUEIDENTIFIER NOT NULL,
    [courseName]         NVARCHAR (64)    NOT NULL,
    [description]        NVARCHAR (4000)  NULL,
    [IsNewCourse]        BIT              NOT NULL,
    [codeType]           INT              NOT NULL,
    [codePlanningMethod] INT              NOT NULL,
    [courseGroupId]      UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [pk_Course] PRIMARY KEY CLUSTERED ([courseId] ASC),
    CONSTRAINT [fk_Course__CourseGroup] FOREIGN KEY ([courseGroupId]) REFERENCES [dbo].[CourseGroup] ([courseGroupId])
);

