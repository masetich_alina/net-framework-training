﻿CREATE TABLE [dbo].[TrainerGroup] (
    [trainerGroupId]   UNIQUEIDENTIFIER NOT NULL,
    [trainerGroupName] NVARCHAR (32)    NOT NULL,
    CONSTRAINT [pk_TrainerGroup] PRIMARY KEY CLUSTERED ([trainerGroupId] ASC),
    CONSTRAINT [ak_TrainerGroup__TrainerGroupName] UNIQUE NONCLUSTERED ([trainerGroupName] ASC)
);

