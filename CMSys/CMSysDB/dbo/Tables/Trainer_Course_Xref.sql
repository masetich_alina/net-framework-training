﻿CREATE TABLE [dbo].[Trainer_Course_Xref] (
    [trainerId] UNIQUEIDENTIFIER NOT NULL,
    [courseId]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [pk_Trainer_Course_Xref] PRIMARY KEY CLUSTERED ([trainerId] ASC, [courseId] ASC),
    CONSTRAINT [fk_Trainer_Course_Xref__Course] FOREIGN KEY ([courseId]) REFERENCES [dbo].[Course] ([courseId]) ON DELETE CASCADE,
    CONSTRAINT [fk_Trainer_Course_Xref__Trainer] FOREIGN KEY ([trainerId]) REFERENCES [dbo].[Trainer] ([trainerId]) ON DELETE CASCADE
);

