﻿CREATE TABLE [dbo].[CourseGroup] (
    [courseGroupId]   UNIQUEIDENTIFIER NOT NULL,
    [courseGroupName] NVARCHAR (32)    NOT NULL,
    CONSTRAINT [pk_CourseGroup] PRIMARY KEY CLUSTERED ([courseGroupId] ASC),
    CONSTRAINT [ak_CourseGroup__CourseGroupName] UNIQUE NONCLUSTERED ([courseGroupName] ASC)
);

