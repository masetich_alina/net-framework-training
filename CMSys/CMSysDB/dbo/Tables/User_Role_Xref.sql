﻿CREATE TABLE [dbo].[User_Role_Xref] (
    [userId] UNIQUEIDENTIFIER NOT NULL,
    [roleId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [pk_User_Role_Xref] PRIMARY KEY CLUSTERED ([userId] ASC, [roleId] ASC),
    CONSTRAINT [fk_User_Role_Xref__Role] FOREIGN KEY ([roleId]) REFERENCES [dbo].[Role] ([roleId]) ON DELETE CASCADE,
    CONSTRAINT [fk_User_Role_Xref__User] FOREIGN KEY ([userId]) REFERENCES [dbo].[User] ([userId]) ON DELETE CASCADE
);

