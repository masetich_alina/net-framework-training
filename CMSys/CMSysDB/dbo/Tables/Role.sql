﻿CREATE TABLE [dbo].[Role] (
    [roleId]   UNIQUEIDENTIFIER NOT NULL,
    [roleName] NVARCHAR (32)    NOT NULL,
    CONSTRAINT [pk_Role] PRIMARY KEY CLUSTERED ([roleId] ASC),
    CONSTRAINT [ak_Role__RoleName] UNIQUE NONCLUSTERED ([roleName] ASC)
);

