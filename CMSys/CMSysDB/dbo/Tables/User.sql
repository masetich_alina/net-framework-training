﻿CREATE TABLE [dbo].[User] (
    [userId]         UNIQUEIDENTIFIER NOT NULL,
    [email]          NVARCHAR (128)   NOT NULL,
    [passwordHash]   NVARCHAR (128)   NOT NULL,
    [passwordSalt]   NVARCHAR (128)   NOT NULL,
    [firstname]      NVARCHAR (128)   NOT NULL,
    [lastname]       NVARCHAR (128)   NOT NULL,
    [startDate]      DATE             NOT NULL,
    [endDate]        DATE             NULL,
    [department]     NVARCHAR (128)   NULL,
    [officeLocation] NVARCHAR (128)   NULL,
    [position]       NVARCHAR (128)   NULL,
    [photo]          VARBINARY (MAX)  NULL,
    CONSTRAINT [pk_User] PRIMARY KEY CLUSTERED ([userId] ASC),
    CONSTRAINT [ak_User__Email] UNIQUE NONCLUSTERED ([email] ASC)
);

