﻿namespace SensorLibrary.Model
{
    public enum TypeSensor
    {
        Pressure,
        Temperature,
        MagneticField
    }
}