﻿using System;

namespace SensorLibrary.Model
{
    public class SingletonIdGenerator
    {
        private static long _id;

        public string Id => _id++.ToString().Substring(8);
        public static SingletonIdGenerator Instance => Nested.Instance;

        private SingletonIdGenerator()
        {
        }

        private class Nested
        {
            public static readonly SingletonIdGenerator Instance = new SingletonIdGenerator();

            static Nested()
            {
                _id = DateTime.Now.Ticks;
            }
        }
    }
}