﻿using System.Threading;

namespace SensorLibrary.Model.State
{
    public class CalibrationState : ModeState
    {
        private Timer _timer;
        
        public override void CountUp(Sensor sensor)
        {
            _timer = new Timer(Measure, sensor, 0, 1000);
        }

        public override void Next(Sensor sensor)
        {
            sensor.State = new WorkState();
            OnValueChange();
            _timer.Dispose();
        }

        public void Measure(object state)
        {
            var sensor = (Sensor)state;
            sensor.MeasuredValue += 1;
            OnValueChange();
        }
    }
}