﻿using System;
using System.Threading;

namespace SensorLibrary.Model.State
{
    public class WorkState : ModeState
    {
        private Timer _timer;
      
        public override void CountUp(Sensor sensor)
        {
            _timer = new Timer(Measure, sensor, 0, sensor.Interval * 1000);
        }

        public override void Next(Sensor sensor)
        {
            sensor.State = new SimpleState();
            _timer.Dispose();
            OnValueChange();
        }

        public void Measure(object state)
        {
            var sensor = (Sensor)state;
            var rand = new Random();
            sensor.MeasuredValue = rand.Next(100);
            OnValueChange();
        }
    }
}