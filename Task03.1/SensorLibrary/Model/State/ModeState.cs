﻿using System;

namespace SensorLibrary.Model.State
{
    public abstract class ModeState
    {
        public event EventHandler ValueChange;
        public abstract void CountUp(Sensor sensor);
        public abstract void Next(Sensor sensor);
       
        public void OnValueChange()
        {
            ValueChange?.Invoke(this, EventArgs.Empty);
        }

        
    }
}