﻿namespace SensorLibrary.Model.State
{
    public class SimpleState : ModeState
    {
        public override void CountUp(Sensor sensor)
        {
            sensor.MeasuredValue = 0;
            OnValueChange();
        }

        public override void Next(Sensor sensor)
        {
            sensor.State = new CalibrationState();
            OnValueChange();
        }
    }
}