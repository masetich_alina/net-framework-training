﻿namespace SensorLibrary.Model
{
    public interface IObserver
    {
        void Update();
    }
}