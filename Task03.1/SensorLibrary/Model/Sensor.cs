﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using SensorLibrary.Model.State;

namespace SensorLibrary.Model
{
    public class Sensor : IObservable
    {
        public string Id { get; set; }
        public int Interval { get; set; }
        public TypeSensor TypeSensor { get; set; }
        [NonSerialized]
        private ModeState _state;

        [XmlIgnore]
        public ModeState State
        {
            get => _state;
            set => _state = value;
        }
        private int _measuredValue;

        private readonly List<IObserver> _observers = new List<IObserver>();

        public int MeasuredValue
        {
            get => _measuredValue;
            set
            {
                _measuredValue = value;
                NotifyObservers();
            }
        }

        public Sensor()
        {
            State = new SimpleState();
        }

        public void Next()
        {
            State.Next(this);
            State.CountUp(this);
        }

        public void CountUp()
        {
            State.CountUp(this);
        }

        public void RegisterObserver(IObserver o)
        {
            _observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            _observers.Remove(o);
        }

        public void NotifyObservers()
        {
            foreach (var observer in _observers)
            {
                observer.Update();
            }
        }

    }
}