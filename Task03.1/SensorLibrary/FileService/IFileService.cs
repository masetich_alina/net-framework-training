﻿using System.Collections.Generic;
using SensorLibrary.Model;

namespace SensorLibrary.FileService
{
    public interface IFileService
    {
        List<Sensor> Open(string filename);
        void Save(string filename, List<Sensor> sensorsList);
    }
}