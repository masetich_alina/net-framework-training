﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using SensorLibrary.Model;

namespace SensorLibrary.FileService
{
    public class XmlFileService : IFileService
    {
        public List<Sensor> Open(string filename)
        {
            var sensors = new List<Sensor>();
            var xs = new XmlSerializer(typeof(List<Sensor>));
            using (var fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                sensors = (List<Sensor>) xs.Deserialize(fs);
            }

            return sensors;
        }

        public void Save(string filename, List<Sensor> sensorsList)
        {
            var xs = new XmlSerializer(typeof(List<Sensor>));
            using (var fs = new FileStream(filename, FileMode.Create))
            {
                xs.Serialize(fs, sensorsList);
            }
        }
    }
}