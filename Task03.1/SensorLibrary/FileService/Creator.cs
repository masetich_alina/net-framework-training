﻿using System;

namespace SensorLibrary.FileService
{
    public class Creator
    {
        public IFileService GetResult(TypeFile type)
        {
            IFileService fileService;
            switch (type)
            {
                case TypeFile.Json:
                    fileService = new JsonFileService();
                    return fileService;
                case TypeFile.Xml:
                    fileService = new XmlFileService();
                    return fileService;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}