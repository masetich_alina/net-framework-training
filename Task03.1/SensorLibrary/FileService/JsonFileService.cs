﻿using System.Collections.Generic;
using System.IO;
using SensorLibrary.Model;
using Newtonsoft.Json;

namespace SensorLibrary.FileService
{
    public class JsonFileService : IFileService
    {
        public List<Sensor> Open(string filename)
        {
            var sensors = new List<Sensor>();
            var json = File.ReadAllText(filename);
            sensors = JsonConvert.DeserializeObject(json) as List<Sensor>; 
            
            return sensors;
        }

        public void Save(string filename, List<Sensor> sensorsList) 
        {
            using (var fs = new StreamWriter(filename))
            {
                var json = JsonConvert.SerializeObject(sensorsList);
                fs.Write(json);
            }
        }
    }
}