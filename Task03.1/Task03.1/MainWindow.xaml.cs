﻿using System.Windows;
using Task03._1.DialogService;
using Task03._1.ViewModel;

namespace Task03._1
{
    /// <inheritdoc />
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ApplicationViewModel(new DefaultDialogService());
        }
        
    }
}
