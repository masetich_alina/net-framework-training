﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using SensorLibrary.Model;

namespace Task03._1.ViewModel
{
    public class SensorViewModel : INotifyPropertyChanged, IObserver
    {
        public Sensor Sensor; 

        public SensorViewModel()
        {
            Sensor = new Sensor();
            Sensor.RegisterObserver(this);
        }

        public SensorViewModel(Sensor sensor)
        {
            Sensor = sensor;
            Sensor.RegisterObserver(this);
        }

        public string Id
        {
            get => SingletonIdGenerator.Instance.Id;
            set
            {
                Sensor.Id = value;
                OnPropertyChanged("Id");
            }
        }

        public TypeSensor Type
        {
            get => Sensor.TypeSensor;
            set
            {
                Sensor.TypeSensor = value;
                OnPropertyChanged("Type");
            }
        }

        public int Interval
        {
            get => Sensor.Interval;
            set
            {
                Sensor.Interval = value;
                OnPropertyChanged("Interval");
            }
        }
       
        public int MeasuredValue => Sensor.MeasuredValue;

        public event PropertyChangedEventHandler PropertyChanged;
        
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public void Update()
        {
            OnPropertyChanged("MeasuredValue");
        }
    }
}
