﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using SensorLibrary.FileService;
using SensorLibrary.Model;
using Task03._1.DialogService;

namespace Task03._1.ViewModel
{
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        private RelayCommand _addCommand;
        private readonly IDialogService _dialogService;
        private RelayCommand _openCommand;
        private RelayCommand _removeCommand;
        private RelayCommand _saveCommand;
        private SensorViewModel _selectedSensor;
        private RelayCommand _switchCommand;
        private TypeFile _typeFile = TypeFile.Json;

        public ObservableCollection<SensorViewModel> Sensors { get; set; }

        public TypeFile TypeFile
        {
            get => _typeFile;                    
            set
            {
                if (_typeFile == value)
                    return;

                _typeFile = value;
                OnPropertyChanged("TypeFile");
                OnPropertyChanged("IsJsonFile");
                OnPropertyChanged("IsXmlFile");
            }
        }

        public bool IsJsonFile
        {
            get => TypeFile == TypeFile.Json;
            set => TypeFile = value ? TypeFile.Json : TypeFile;
        }

        public bool IsXmlFile
        {
            get => TypeFile == TypeFile.Xml;
            set => TypeFile = value ? TypeFile.Xml : TypeFile;
        }

        public RelayCommand RemoveCommand
        {
            get
            {
                return _removeCommand ??
                       (_removeCommand = new RelayCommand(obj =>
                       {
                           if (obj is SensorViewModel sensor)
                           {
                               Sensors.Remove(sensor);
                           }
                       },
                           obj => Sensors.Count > 0));
            }
        }

        public RelayCommand SwitchCommand
        {
            get
            {
                return _switchCommand ??
                       (_switchCommand = new RelayCommand(obj =>
                       {
                           if (SelectedSensor == null)
                           {
                               MessageBox.Show("Select sensor");
                               return;
                           }
                           SelectedSensor.Sensor.Next();
                       }));
            }
        }

        public RelayCommand SaveCommand
        {
            get
            {
                return _saveCommand ??
                       (_saveCommand = new RelayCommand(obj =>
                       {
                           try
                           {
                               if (_dialogService.SaveFileDialog() != true)
                               {
                                   return;
                               }
                               var creator = new Creator();
                               var fileService = creator.GetResult(TypeFile);
                               var sensors = new List<Sensor>();
                               foreach (var s in Sensors.ToList())
                               {
                                   sensors.Add(s.Sensor);
                               }
                               fileService.Save(_dialogService.FilePath, sensors);
                               _dialogService.ShowMessage("File save");
                           }
                          
                           catch (Exception ex)
                           {
                               _dialogService.ShowMessage(ex.Message);
                           }
                       }));
            }
        }

        public RelayCommand OpenCommand
        {
            get
            {
                return _openCommand ??
                       (_openCommand = new RelayCommand(obj =>
                       {
                           try
                           {
                               if (_dialogService.OpenFileDialog() != true)
                               {
                                   return;
                               }
                               var creator = new Creator();
                               var fileService = creator.GetResult(TypeFile);
                               var sensors = fileService.Open(_dialogService.FilePath);
                               Sensors.Clear();
                               foreach (var s in sensors)
                               {
                                   Sensors.Add(new SensorViewModel(s));       

                               }
                               _dialogService.ShowMessage("File open");
                           }
                           catch (Exception ex)
                           {
                               _dialogService.ShowMessage(ex.Message);
                           }
                       }));
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                return _addCommand ??
                       (_addCommand = new RelayCommand(obj =>
                       {
                           var sensor = new SensorViewModel();
                           Sensors.Insert(0, sensor);
                           SelectedSensor = sensor;
                           SelectedSensor.Sensor.CountUp();
                       }));
            }
        }

        public SensorViewModel SelectedSensor
        {
            get => _selectedSensor;
            set
            {
                _selectedSensor = value;
                OnPropertyChanged("SelectedSensor");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ApplicationViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;
            Sensors = new ObservableCollection<SensorViewModel>();
        }

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}