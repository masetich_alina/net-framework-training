﻿namespace Task03._1.DialogService
{
    public interface IDialogService
    {
        string FilePath { get; set; }
        void ShowMessage(string message);
        bool OpenFileDialog();
        bool SaveFileDialog();
    }
}