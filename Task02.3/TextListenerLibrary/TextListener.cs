﻿using System;
using System.Collections.Generic;
using System.IO;
using ListenerLibrary;

namespace TextListenerLibrary
{
    public class TextListener : IListener
    {
        private Dictionary<string, string> _dict;

        public void Write(string message, LogLevel? level)
        {
            string textLevel;
            switch (level)
            {
                case LogLevel.Trace:
                    textLevel = " [TRACE] ";
                    break;
                case LogLevel.Information:
                    textLevel = " [INFO] ";
                    break;
                case LogLevel.Debug:
                    textLevel = " [DEBUG] ";
                    break;
                case LogLevel.Warning:
                    textLevel = " [WARNING] ";
                    break;
                case LogLevel.Error:
                    textLevel = " [ERROR] ";
                    break;
                case LogLevel.Fatal:
                    textLevel = " [FATAL] ";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }

            var fileName = string.Empty;
            foreach (var attr in _dict)
            {
                fileName = attr.Key == "filename" ? attr.Value : "log.txt";
            }
            var path = Directory.GetCurrentDirectory() + "\\" + fileName;

            using (var file = new StreamWriter(path, true))
            {
                file.WriteLine(message + textLevel);
            }
        }

        public void Initialize(Dictionary<string, string> attributes)
        {
            _dict = attributes;
        }
    }
}