﻿using System.Collections.Generic;
using System.Diagnostics;
using ListenerLibrary;

namespace EventLogListenerLibrary
{
    public class EventLogListener : IListener
    {
        private Dictionary<string, string> _dict;
        public void Write(string message, LogLevel? level)
        {
            var name = string.Empty;
            foreach (var attr in _dict)
            {
                if (attr.Key == "name")
                {
                    name = attr.Value;
                }
            }
            using (var eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";

                if (level == LogLevel.Warning)
                {
                    eventLog.WriteEntry(message, EventLogEntryType.Warning);
                }

                if (level == LogLevel.Error)
                {
                    eventLog.WriteEntry(message, EventLogEntryType.Error);
                }
                else
                {
                    eventLog.WriteEntry(message);
                }
            }
        }

        public void Initialize(Dictionary<string, string> attributes)
        {
            _dict = attributes;
        }
    }
}