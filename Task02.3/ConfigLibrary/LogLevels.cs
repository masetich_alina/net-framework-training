﻿namespace ConfigLibrary
{
    public enum TypesLogLevels
    {
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal
    }
}
