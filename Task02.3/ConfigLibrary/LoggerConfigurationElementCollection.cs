﻿using System.Configuration;

namespace ConfigLibrary
{
    public class LoggerConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new LoggerConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as LoggerConfigurationElement)?.Name;
        }
    }
}
