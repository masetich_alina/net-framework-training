﻿using System.Configuration;

namespace ConfigLibrary
{
    public class LogConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("listeners")]
        [ConfigurationCollection(typeof(ListenerConfigurationElementCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]

        public ListenerConfigurationElementCollection Listeners
        {
            get => this["listeners"] as ListenerConfigurationElementCollection;
            set => this["listeners"] = value;
        }

        [ConfigurationProperty("loggers")]
        [ConfigurationCollection(typeof(LoggerConfigurationElementCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]

        public LoggerConfigurationElementCollection Loggers
        {
            get => this["loggers"] as LoggerConfigurationElementCollection;
            set => this["loggers"] = value;
        }
    }
}
