﻿using System.Configuration;

namespace ConfigLibrary
{
    public class ListenerConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ListenerConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as ListenerConfigurationElement).Name;
        }
    }
}
