﻿using System;
using System.Configuration;

namespace ConfigLibrary
{
    public class LoggerConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get => Convert.ToString(this["name"]);
            set => this["name"] = value;
        }

        [ConfigurationProperty("minlevel")]
        public TypesLogLevels? MinLevel
        {
            get => this["minlevel"] as TypesLogLevels?;
            set => this["minlevel"] = value;
        }

        [ConfigurationProperty("writeTo")]
        public string Filename
        {
            get => Convert.ToString(this["writeTo"]);
            set => this["writeTo"] = value;
        }

        public LoggerConfigurationElement()
        {
        }
    }
}
