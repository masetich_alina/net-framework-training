﻿using System;
using System.Configuration;

namespace ConfigLibrary
{
    public class ListenerConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get => Convert.ToString(this["name"]);
            set => this["name"] = value;
        }

        [ConfigurationProperty("type")]
        public string Type
        {
            get => Convert.ToString(this["type"]);
            set => this["type"] = value;
        }

        [ConfigurationProperty("filename")]
        public string Filename
        {
            get => Convert.ToString(this["filename"]);
            set => this["filename"] = value;
        }

        public ListenerConfigurationElement()
        {
        }
    }
}
