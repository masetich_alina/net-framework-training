﻿using System.Collections.Generic;
using System.IO;
using ListenerLibrary;
using Xceed.Words.NET;

namespace WordListenerLibrary
{
    public class WordListener : IListener
    {
        private Dictionary<string, string> _dict;
        public void Write(string message, LogLevel? level)
        {
            var fileName = string.Empty;
            foreach (var attr in _dict)
            {
                fileName = attr.Key == "filename" ? attr.Value : "log.docx";
            }
            var path = Directory.GetCurrentDirectory() + "\\" + fileName;
            if (!File.Exists(path))
            {
                var doc = DocX.Create(path);
                doc.InsertParagraph(message);
                doc.Save();
            }
            else
            {
                var doc = DocX.Load(path);
                doc.InsertParagraph(message);
                doc.Save();
            }
        }

        public void Initialize(Dictionary<string, string> attributes)
        {
            _dict = attributes;
        }
    }
}