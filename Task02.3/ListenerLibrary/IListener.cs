﻿using System.Collections.Generic;

namespace ListenerLibrary
{
    public interface IListener
    {
        void Write(string message, LogLevel? level);
        void Initialize(Dictionary<string, string> attributes);
    }
}