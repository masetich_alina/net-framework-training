﻿using System.Configuration;

namespace LoggerLibrary
{
    public class ListenerConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ListenerConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as ListenerConfigurationElement);
        }
    }
}