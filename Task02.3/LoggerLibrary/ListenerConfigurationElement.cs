﻿using System;
using System.Configuration;

namespace LoggerLibrary
{
    public class ListenerConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("type", IsRequired = true)]
        public string Type => Convert.ToString(this["type"]);

        [ConfigurationProperty("items")]
        [ConfigurationCollection(typeof(ItemConfigurationElementCollection), AddItemName = "add")]
        public ItemConfigurationElementCollection Items => this["items"] as ItemConfigurationElementCollection;

    }
}