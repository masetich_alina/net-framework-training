﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ListenerLibrary;

namespace LoggerLibrary
{
    public class Helper
    {
        public static void Handler(Logger logger)
        {
            if (!(ConfigurationManager.GetSection("log") is LogConfigurationSection section))
            {
                return;
            }
            logger.MinLevel = section.Logger.Minlevel;

            foreach (ListenerConfigurationElement l in section.Listeners)
            {
                var dictGlobal = new Dictionary<IListener, Dictionary<string, string>>();
                var split = l.Type.Split(',', ' ');
                var dict = new Dictionary<string, string>();
                var assembly = Assembly.Load(split[2]);

                foreach (ItemConfigurationElement item in l.Items)
                {
                    dict.Add(item.Key, item.Value);
                }

                dictGlobal.Add(
                    (IListener) assembly.CreateInstance(split[0]) ?? throw new InvalidOperationException(),
                    dict);
                logger.Add(dictGlobal);
            }
        }
    }
}