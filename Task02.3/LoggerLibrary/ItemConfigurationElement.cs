﻿using System;
using System.Configuration;

namespace LoggerLibrary
{
    public class ItemConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("key", IsRequired = true)]
        public string Key => Convert.ToString(this["key"]);

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value => Convert.ToString(this["value"]);
    }
}
