﻿using System;
using System.Configuration;

namespace LoggerLibrary
{
    public class ItemConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ItemConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as ItemConfigurationElement);
        }
    }
}
