﻿using System.Configuration;

namespace LoggerLibrary
{
    public class LogConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("listeners")]
        [ConfigurationCollection(typeof(ListenerConfigurationElementCollection), AddItemName = "add")]
        public ListenerConfigurationElementCollection Listeners => this["listeners"] as ListenerConfigurationElementCollection;

        [ConfigurationProperty("logger")]
        public LoggerConfigurationElement Logger => (LoggerConfigurationElement) base["logger"];

        
    }
}