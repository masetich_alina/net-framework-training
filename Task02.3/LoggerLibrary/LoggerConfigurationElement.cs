﻿using System;
using System.Configuration;
using ListenerLibrary;

namespace LoggerLibrary
{
    public class LoggerConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("minlevel", IsRequired = true)]
        public LogLevel? Minlevel
        {
            get => (this["minlevel"]) as LogLevel?;
            set => this["minlevel"] = value;
        }
    }
}
