﻿using System;
using System.Collections.Generic;
using System.Text;
using AttributeLibrary;
using ListenerLibrary;

namespace LoggerLibrary
{
    public class Logger
    {
        private readonly List<Dictionary<IListener, Dictionary<string, string>>> _listeners
            = new List<Dictionary<IListener, Dictionary<string, string>>>();

        public LogLevel? MinLevel { get; set; }

        public Logger()
        {
            Helper.Handler(this);
        }

        public void Track(object obj)
        {
            var type = obj.GetType();
            var dict = new Dictionary<string, object>();
            if (!Attribute.IsDefined(type, typeof(TrackingEntityAttribute)))
            {
                return;
            }

            foreach (var f in type.GetFields())
            {
                if (!Attribute.IsDefined(f, typeof(TrackingPropertyAttribute)))
                {
                    return;
                }
                var attr = Attribute.GetCustomAttribute(f, typeof(TrackingPropertyAttribute));
                dict.Add(((TrackingPropertyAttribute) attr).Name ?? f.Name, f.GetValue(obj));
            }

            foreach (var f in type.GetProperties())
            {
                if (!Attribute.IsDefined(f, typeof(TrackingPropertyAttribute)))
                {
                    return;
                }
                var attr = Attribute.GetCustomAttribute(f, typeof(TrackingPropertyAttribute));
                dict.Add(((TrackingPropertyAttribute) attr).Name ?? f.Name, f.GetValue(obj));
            }

            var text = new StringBuilder();
            foreach (var pairs in dict)
            {
                text.Append("Key = " + pairs.Key + ", Value = " + pairs.Value);
            }
            WriteFormattedLog(MinLevel, text.ToString());
        }
        
        public void Add(Dictionary<IListener, Dictionary<string, string>> dict)
        {
            _listeners.Add(dict);
        }

        public void Debug(string text)
        {
            WriteFormattedLog(LogLevel.Debug, text);
        }

        public void Error(string text)
        {
            WriteFormattedLog(LogLevel.Error, text);
        }

        public void Fatal(string text)
        {
            WriteFormattedLog(LogLevel.Fatal, text);
        }

        public void Info(string text)
        {
            WriteFormattedLog(LogLevel.Information, text);
        }

        public void Trace(string text)
        {
            WriteFormattedLog(LogLevel.Trace, text);
        }

        public void Warning(string text)
        {
            WriteFormattedLog(LogLevel.Warning, text);
        }

        private void WriteFormattedLog(LogLevel? level, string text)
        {
            if (level < MinLevel)
            {
                return;
            }
            foreach (var l in _listeners)
            {
                foreach (var item in l)
                {
                    item.Key.Initialize(item.Value);
                    item.Key.Write(text, level);
                }
            }
        }
    }
}