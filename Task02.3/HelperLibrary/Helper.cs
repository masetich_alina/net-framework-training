﻿using System.Configuration;
using LoggerLibrary;
using ListenerLibrary;

namespace HelperLibrary
{
    public class Helper
    {
        private readonly LogConfigurationSection _section = ConfigurationManager.GetSection("log") as LogConfigurationSection;

        public void Handler()
        {
            var logger = new Logger(_section.Logger.Minlevel);
            logger.Log();
            foreach (ListenerConfigurationElement s in _section.Listeners)
            {
                Logger.Add(new Listener(s.Name, s.Type, s.Filename));
            }
        }      
    }
}
