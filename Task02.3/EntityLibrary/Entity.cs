﻿using AttributeLibrary;

namespace EntityLibrary
{
    [TrackingEntity]
    public class Entity
    {
        public int field1;

        [TrackingProperty] public int field2;

        [TrackingProperty("NewName")] public int Property1 { get; set; }

        public int Property2 { get; set; }

        [TrackingProperty] public int Property3 { get; set; }

        public Entity(int f1, int f2, int property1, int property2, int property3)
        {
            field1 = f1;
            field2 = f2;
            Property1 = property1;
            Property2 = property2;
            Property3 = property3;
        }

    }
}