﻿using System;

namespace AttributeLibrary
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class TrackingPropertyAttribute : Attribute
    {
        public string Name { get; }
        public TrackingPropertyAttribute(string name = null)
        {
            Name = name;
        }
    }
}