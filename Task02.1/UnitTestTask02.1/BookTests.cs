﻿using System;
using System.Text;
using Task02._1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestTask02._1
{
    [TestClass]
    public class BookTests
    {
        private const string Isbn = "1234567890123";
        private const string Name = "The old man and the sea";
        Book book = new Book(Isbn, Name);

        [TestMethod]
        public void EqualFields_Name()
        {
            Assert.AreEqual(Name, book.Name);
        }

        [TestMethod]
        public void EqualFields_Isbn()
        {
            Assert.AreEqual(Isbn, book.Isbn);
        }

        [TestMethod]
        public void EqualToStringReturn()
        {
            Assert.AreEqual(Name, book.ToString());
        }

        [TestMethod]
        public void EqualBooks()
        {
            var book1 = new Book(Isbn, "Other name");
            Assert.AreEqual(true, book1.Equals(book));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateObject_WithNameNull()
        {
            var book1 = new Book(Isbn, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CreateObject_WithNameMoreThen200()
        {
            var name = new StringBuilder().Append('a', 201).ToString();
            var author1 = new Author(name, Isbn);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateObject_WithFirstIsbnNull()
        {
            var book1 = new Book(null, Name);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateObject_WithErrorIsbnFormat()
        {
            var author1 = new Book("12345678901231", Name);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateObject_WithErrorDate()
        {
            var author1 = new Book(Isbn, Name, null, new DateTime(2020, 12, 21));
        }


    }
}
