﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task02._1;

namespace UnitTestTask02._1
{
    [TestClass]
    public class AuthorTests
    {
        private const string Name = "Ernest";
        private const string Surname = "Hemingway";
        Author author = new Author(Name, Surname);

        [TestMethod]
        public void EqualFields_FirstName()
        {
            Assert.AreEqual(Name, author.FirstName);
        }

        [TestMethod]
        public void EqualFields_LastName()
        {
            Assert.AreEqual(Surname, author.LastName);
        }

        [TestMethod]
        public void EqualToStringReturn()
        {
            Assert.AreEqual(Name + " " + Surname, author.ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CreateObject_WithFirstNameMoreThen200()
        {
            var firstName = new StringBuilder().Append('a', 201).ToString();
            var author1 = new Author(firstName, Surname); 
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CreateObject_WithLastNameMoreThen200()
        {
            var lastName = new StringBuilder().Append('a', 201).ToString();
            var author1 = new Author(Name, lastName);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateObject_WithFirstNameNull()
        {
            var author1 = new Author(null, Surname);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateObject_WithLastNameNull()
        {
            var author1 = new Author(Name, null);
        }
    }
}
