﻿using System;
using System.Text;
using Task02._1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace UnitTestTask02._1
{
    [TestClass]
    public class CatalogTests
    {
        Book book1 = new Book("1234567891230", "book1");
        Book book2 = new Book("1234567891453", "book2");
        Catalog catalog = new Catalog();

        [TestMethod]
        public void Count_AddTwoElements_Return2() 
        {
            catalog.Add(book1);
            catalog.Add(book2);

            Assert.AreEqual(2, catalog.Count());
        }

        [TestMethod]
        public void ContainsBookInCatalog()
        {
            catalog.Add(book1);
            Assert.IsTrue(catalog.ContainsBook(book1));
        }


    }
}
