﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Task02._1
{
    public class Catalog : IEnumerable<Book>
    {
        private readonly Dictionary<string, Book> _catalogDictionary;

        public Book this[string i]
        {
            get => _catalogDictionary[Book.NormalizeIsbn(i)];
            set => _catalogDictionary[Book.NormalizeIsbn(i)] = value;
        }

        public Catalog()
        {
            _catalogDictionary = new Dictionary<string, Book>();
        }

        public IEnumerator<Book> GetEnumerator()
        {
            var books = _catalogDictionary.Values.OrderBy(x => x.Name);
            return books.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count()
        {
            return _catalogDictionary.Count();
        }

        public bool ContainsBook(Book book)
        {
            return _catalogDictionary.ContainsValue(book);
        }

        public void Add(Book book)
        {
            _catalogDictionary[book.Isbn] = book;
        }

        public IOrderedEnumerable<Book> GetBooksSortedByDate()
        {
            var sortedBooks = this.OrderByDescending(x => x.Date);
            return sortedBooks;
        }

        public IEnumerable<Book> GetBooksByAuthor(string name, string surname)
        {
            var author = new Author(name.ToUpper(), surname.ToUpper());
            var book = this.Where(b => b.Any(a => a.Equals(author)));
            return book;
        }

        public IEnumerable<(Author, int)> GetTuple()
        {
            var temp = this.SelectMany(b => b, (b, a) => (a, count: this.Count(b1 => b1.Contains(a)))).Distinct();
            return temp;
        }
    }
}