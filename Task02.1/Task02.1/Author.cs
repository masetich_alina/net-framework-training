﻿using System;

namespace Task02._1
{
    public class Author
    {
        private const int Limit = 200;
        private string _firstName;
        private string _lastName;

        public string FirstName
        {
            get => _firstName;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value), "Firstname must not be null");
                }

                if (value.Length > Limit)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Firstname must not be more than 200");
                }

                _firstName = value.ToUpper();
            }
        }

        public string LastName
        {
            get => _lastName;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value), "Lastname must not be null");
                }

                if (value.Length > Limit)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Lastname must not be more than 200");
                }

                _lastName = value.ToUpper();
            }
        }

        public Author(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Author author))
            {
                return false;
            }
            return author.ToString() == ToString();
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}