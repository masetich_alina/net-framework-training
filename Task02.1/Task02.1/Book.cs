﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace Task02._1
{
    public class Book : IEnumerable<Author>
    {
        private const string IsbnFormat = @"^(\d{3}-\d{1}-\d{2}-\d{6}-\d{1})$|^(\d{13})$";
        private const int Limit = 1000;

        private readonly ReadOnlyCollection<Author> _authors;
        private DateTime? _date;
        private string _isbn;
        private string _name;

        public DateTime? Date
        {
            get => _date;
            set
            {
                if (value >= DateTime.Now)
                {
                    throw new ArgumentException("Publication date must not be more than the current date");
                }

                _date = value;
            }
        }

        public string Isbn
        {
            get => _isbn;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value), "Isbn must not be null");
                }

                if (!Regex.IsMatch(value, IsbnFormat))
                {
                    throw new ArgumentException(
                        "Wrong isbn format. It should be \"XXX-X-XX-XXXXXX-X\" or \"XXXXXXXXXXXXX\", where X - digit 0..9");
                }

                _isbn = NormalizeIsbn(value);
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value), "Name must not be null");
                }

                if (value.Length > Limit)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Name must not be more than 200");
                }

                _name = value.ToUpper();
            }
        }

        public Book(string isbn, string name, List<Author> authors = null, DateTime? date = null)
        {
            Isbn = Regex.Replace(isbn, "-", "");
            Name = name;
            _authors = new ReadOnlyCollection<Author>(authors ?? new List<Author>());
            Date = date;
        }

        public IEnumerator<Author> GetEnumerator()
        {
            return _authors.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static string NormalizeIsbn(string isbn)
        {
            var regex = new Regex(@"[^\d]");
            return regex.Replace(isbn, "");
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Book book))
            {
                return false;
            }
            return Isbn == book.Isbn;
        }

        public override int GetHashCode()
        {
            return Isbn.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}