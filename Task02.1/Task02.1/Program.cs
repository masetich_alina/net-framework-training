﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task02._1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var author1 = new Author("firstName1", "lastName1");
            var author2 = new Author("firstName2", "lastName2");
            var author3 = new Author("firstName3", "lastName3");

            var authors = new List<Author> {author1, author2};

            var book1 = new Book("1234567890123", "5AbCbook", null, new DateTime(2011, 12, 21));
            var book2 = new Book("123-4-56-789012-3", "1Abook", authors, new DateTime(2014, 12, 21));
            var book3 = new Book("123-4-56-712312-4", "book3", authors, new DateTime(2010, 12, 21));
        
            Console.WriteLine(book1.Equals(book2));
            Console.WriteLine();

            var catalog = new Catalog {book1, book2, book3};

            foreach (var c in catalog)
            {
                Console.WriteLine(c);
            }
            Console.WriteLine();

            foreach (var c in catalog.GetBooksSortedByDate())
            {
                Console.WriteLine(c + " " + c.Date);
            }
            Console.WriteLine();

            foreach (var c in catalog.GetBooksByAuthor("FIRSTNAME1", "LASTNAME1"))
            {
                Console.WriteLine(c);
            }

            Console.WriteLine();

            foreach (var c in catalog.GetTuple())
            {
                Console.WriteLine(c);
            }
            Console.WriteLine();

            Console.WriteLine(author1.Equals(author3));

            Console.WriteLine(catalog["123-4-56-712312-4"]);
        }
    }
}

