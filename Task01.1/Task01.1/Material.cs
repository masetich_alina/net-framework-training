﻿using System;

namespace Task01._1
{
    internal abstract class Material : Entity
    {
        protected Material(string description) : base(description){}

        public abstract Material Clone();

    }
}
