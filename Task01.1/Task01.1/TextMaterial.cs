﻿using System;

namespace Task01._1
{
    internal class TextMaterial : Material
    {
        private const int Limit = 10000;
        private string _text;        

        public string Text
        {
            get => _text;
            set
            {
                _text = value ?? throw new ArgumentNullException(nameof(value), $"Text must not be null");

                if (value.Length > Limit)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), $"Text must not be more than 10000");
                }
                
                _text = value;
            }
        }

        public TextMaterial(string text, string description) : base(description)
        {
            Text = text;
        }

        public override Material Clone()
        {
            return new TextMaterial(Text = Text, Description = Description);
        }
    }
}