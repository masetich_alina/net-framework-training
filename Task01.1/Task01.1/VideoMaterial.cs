﻿using System;

namespace Task01._1
{
    internal class VideoMaterial : Material, IVersionable
    {
        private string _uriVideo;
        private byte[] _version = new byte[8];
        private VideoFormat _videoFormat;

        public string UriImage { get; set; }

        public string UriVideo
        {
            get => _uriVideo;
            set => _uriVideo = value ?? throw new ArgumentNullException(nameof(value), $"Uri video must not be null");
        }

        public VideoMaterial(string description, string uriVideo, string uriImage, VideoFormat videoFormat) :
            base(description)
        {
            UriVideo = uriVideo;
            UriImage = uriImage;
            _videoFormat = videoFormat;
        }

        public byte[] ReadVersion()
        {
            return _version;
        }

        public void InstallVersion(byte[] arrayBytes)
        {
            _version = arrayBytes;
        }

        public override Material Clone()
        {
            var videoFormat = new VideoFormat();
            return new VideoMaterial(
                Description = Description,
                UriVideo = UriVideo,
                UriImage = UriImage,
                this._videoFormat = videoFormat);
        }
    }
}