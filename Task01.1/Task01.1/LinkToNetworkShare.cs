﻿using System;

namespace Task01._1
{
    internal class LinkToNetworkShare : Material
    {
        private LinkType _linkType;
        private string _uriContent;

        public LinkToNetworkShare(string description, string uriContent, LinkType linkType) :
            base(description)
        {
            UriContent = uriContent;
            _linkType = linkType;
        }

        public string UriContent
        {
            get => _uriContent;
            set => _uriContent = value ?? throw new ArgumentNullException($"Uri content must not be null");
        }

        public override Material Clone()
        {
            var linkType = new LinkType();
            return new LinkToNetworkShare(
                Description = Description,
                UriContent = UriContent,
                _linkType = linkType);
        }
    }
}