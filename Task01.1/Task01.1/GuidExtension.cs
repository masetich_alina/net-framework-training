﻿using System;

namespace Task01._1
{
    public static class GuidExtension
    {
        public static void GeneratorGuid(this Entity entity) => entity.Id = Guid.NewGuid();
    }
}
