﻿using System;
using System.Linq;

namespace Task01._1
{
    internal class TrainingLesson : Entity, IVersionable, ICloneable
    {
        public Material[] _materials;
        private byte[] _version = new byte[8];

        public TrainingLesson(string description)
            : base(description)
        {
            _materials = new Material[0];
        }

        public object Clone()
        {
            var materials = _materials.Select(x => x).ToArray();

            foreach (var material in materials)
            {
                material.Clone();
            }
                
            return new TrainingLesson(Description = Description)
            {
                _materials = materials,
                _version = _version
            };
        }

        public byte[] ReadVersion()
        {
            return _version;
        }

        public void InstallVersion(byte[] arrayBytes)
        {
            _version = arrayBytes;
        }

        public void AddType(Material obj)
        {
            Array.Resize(ref _materials, _materials.Length + 1);
            _materials[_materials.Length - 1] = obj ?? throw new Exception("Wrong type of material");
        }

        public MaterialType DefineTypeLesson()
        {
            foreach (var lesson in _materials)
            {
                if (lesson is VideoMaterial)
                {
                    return MaterialType.VideoMaterial;
                }              
            }

            return MaterialType.TextMaterial;
        }
    }
}