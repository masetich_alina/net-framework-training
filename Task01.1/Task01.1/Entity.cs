﻿using System;

namespace Task01._1
{
    public class Entity
    {
        private const int Limit = 256;
        private string _description;
        private Guid _id;
        
        public Guid Id { get; set; }

        public string Description
        {
            get => _description;
            set
            {
                if (value != null && value.Length > Limit)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Text must not be more than 256");
                }
                   
                _description = value;
            }
        }

        public Entity(string description)
        {
            this.GeneratorGuid();
            Description = description;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Entity lesson)) return false;

            return _id == lesson._id;
        }

        public override int GetHashCode()
        {
            return _id.GetHashCode();
        }

        public override string ToString()
        {
            return Description;
        }

       
    }
}