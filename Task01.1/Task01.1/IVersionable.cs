﻿namespace Task01._1
{
    internal interface IVersionable
    {
        byte[] ReadVersion();
        void InstallVersion(byte[] arrayBytes); 
    }
}
