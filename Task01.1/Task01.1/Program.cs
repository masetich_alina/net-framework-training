﻿using System;

namespace Task01._1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var lessonMaterials = new Entity("some lesson");

            var textMaterial = new TextMaterial("sd", "textMaterial");
            var textMaterial2 = new TextMaterial("werft", "textMaterial2");

            var videoMaterial = new VideoMaterial("videoMaterial", "asdf", "adsf", VideoFormat.Mp4);

            var linkToNetworkShare = new LinkToNetworkShare("asder", "swedfg", LinkType.Audio);

            var trainingLesson = new TrainingLesson("first lesson");
            trainingLesson.AddType(textMaterial);
            trainingLesson.AddType(textMaterial2);
            trainingLesson.AddType(videoMaterial);
            
            Console.WriteLine();
            Console.WriteLine(trainingLesson.DefineTypeLesson());

            //videoMaterial.InstallVersion();
            //Console.WriteLine(videoMaterial.ReadVersion());

            var qww = (TrainingLesson)trainingLesson.Clone();
            trainingLesson.AddType(linkToNetworkShare);
            foreach (var a in trainingLesson._materials)
            {
                Console.WriteLine(a);
            }
            Console.WriteLine();
            foreach (var a in qww._materials)
            {
                Console.WriteLine(a);
            }
        }
    }
}

